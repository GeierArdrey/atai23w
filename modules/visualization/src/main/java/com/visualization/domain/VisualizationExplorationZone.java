package com.visualization.domain;

import com.shared.domain.Point2d;
import com.shared.domain.ZoneExplorationState;

public interface VisualizationExplorationZone {
    Point2d getZoneCenter();
    ZoneExplorationState getExplorationState();
}
