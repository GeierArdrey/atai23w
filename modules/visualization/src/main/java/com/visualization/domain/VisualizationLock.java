package com.visualization.domain;

public interface VisualizationLock {
    String getMachine();
}
