(define (problem task)
(:domain rcll)
(:objects
    start - location
    bs - base_station
    r1 - robot
p1  - product
)
(:init
(at r1 start)
(free r1)
(n_holding r1)
(empty bs)
(stage_c0_0 p1)
)
(:goal (and
(delivered p1)
)
)
(:metric minimize  (total-time) )
