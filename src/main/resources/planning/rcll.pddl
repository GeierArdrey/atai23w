(define (domain rcll)
 (:requirements :strips :typing :durative-actions :fluents)
 (:types base_station cap_station_input cap_station_output delivery_station starting - location
   location holding_station - station
   cap_station - holding_station
   robot product ccolor rcolor numbers)

  (:predicates
	(at ?r - robot ?l - location)
	(holdafter ?p - product)
	(free ?r - robot)
	(empty ?l - location)
	(holding ?r - robot ?p - product)
	(n_holding ?r - robot)
	(holding_base ?r - robot)
	(holding_capbase ?r - robot)
	(station_holding ?cs - holding_station ?p - product)
	(n_station_holding ?cs - holding_station)
	(cs_station_has_capbase ?cs - cap_station)
	(n_cs_station_has_capbase ?cs - cap_station)
	(parent_cs ?l - location ?cs - cap_station)
	(stage_c0_0 ?p - product)
	(stage_c0_1 ?p - product)
	(stage_c0_2 ?p - product)
	(stage_c0_3 ?p - product)
	(stage_c0_4 ?p - product)
	(todeliver ?p - product)
	(delivered ?p - product)
	(capcolor ?p - product ?c - ccolor)
	(stationcapcolor ?cs - cap_station ?c - ccolor)
	(basetodispose ?cs - cap_station)
	(n_basetodispose ?cs - cap_station)
	(tocheck ?p - product)
	(complexity0 ?p - product)
	(complexity1 ?p - product)
	(complexity2 ?p - product)
	(complexity3 ?p - product)
	(requiredbases ?c - rcolor  ?x - numbers)
	(increasebyone ?x - numbers ?res - numbers)
	(subtract ?x1 - numbers ?x2 - numbers ?res - numbers)

 )

 (:functions
	(distance ?l1 ?l2 - location)
	(deliveryWindowStart ?p - product)
 )

 (:durative-action waitForDeliveryWindow
	:parameters (?p - product)
	:duration (= ?duration (deliveryWindowStart ?p) )
	:condition (and (at start (tocheck ?p)))
	:effect (and
		(at end (holdafter ?p))
		(at end (not (tocheck ?p)))
 	)
)


(:durative-action move
	:parameters (?r - robot ?from - location  ?to - location )
	:duration (= ?duration (distance ?from ?to))
	:condition (and (at start (at ?r ?from)) (at start (free ?r)) (at start (empty ?to) )  )
	:effect (and
             (at start (not (at ?r ?from)))
             (at end (at ?r ?to))
	     (at start (not (free ?r)))
	     (at end (free ?r))
	     (at start (not (empty ?to)))
	     (at start (empty ?from))
 	)
)



(:durative-action getBaseFromBScriticalTask
	:parameters (?r - robot ?bs - base_station ?p - product)
	:duration (= ?duration  20 )
	:condition (and
		(at start (at ?r ?bs))
		(at start (free ?r))
		(at start (stage_c0_0 ?p))
		(at start (n_holding ?r))
	)
	:effect (and
		(at start (not (free ?r)))
		(at end (free ?r))
		(at end (stage_c0_1 ?p ))
		(at end (not (stage_c0_0 ?p)))
		(at end (holding ?r ?p))
		(at start (not (n_holding ?r)))

 	)
)



(:durative-action deliverProductC0ToCScriticalTask
	:parameters (?r - robot ?csi - cap_station_input  ?cs - cap_station ?p - product ?c - ccolor)
	:duration (= ?duration  20 )
	:condition (and
		(at start (at ?r ?csi))
		(at start (free ?r))
		(at start (parent_cs ?csi ?cs))
		(at start (holding ?r ?p))
		(at start (stage_c0_1 ?p))
		(at start (complexity0 ?p))
		(at start (n_basetodispose ?cs))

		(at start (n_station_holding ?cs))
		(at start (cs_station_has_capbase ?cs))

		(at start (capcolor ?p ?c))
		(at start (stationcapcolor ?cs ?c))
	)
	:effect (and
		(at start (not (free ?r)))
		(at start (not (stage_c0_1 ?p)))
		(at end (free ?r))
		(at end (not (holding ?r ?p)))
		(at end (n_holding ?r))
		(at end (station_holding ?cs ?p))
		(at end (not (n_station_holding ?cs)))
	)
)


(:durative-action getCapBaseFromCSresourceTask
	:parameters (?r - robot  ?csi - cap_station_input ?cs - cap_station )
	:duration (= ?duration  25 )
	:condition (and
		(at start (at ?r ?csi))
		(at start (free ?r))
		(at start (parent_cs ?csi ?cs))
		(at start (n_holding ?r))
		(at start (n_cs_station_has_capbase ?cs))
		(at start (n_station_holding ?cs))

	)
	:effect (and
		(at start (not (free ?r)))
		(at end (free ?r))
		(at end (cs_station_has_capbase ?cs))
		(at end (not (n_cs_station_has_capbase ?cs)))
		(at end (basetodispose ?cs))
		(at end (not (n_basetodispose ?cs)))
	)
)



(:durative-action getBaseFromCSResourceTask
	:parameters (?r - robot ?cso - cap_station_output ?cs - cap_station)
	:duration (= ?duration  20 )
	:condition (and
		(at start (at ?r ?cso))
		(at start (free ?r))
		(at start (parent_cs ?cso ?cs))
		(at start (n_holding ?r))
		(at start (basetodispose ?cs))
	)
	:effect (and
		(at start (not (free ?r)))
		(at end (free ?r))
		(at end (holding_base ?r))
		(at start (not (n_holding ?r)))
		(at end (not (basetodispose ?cs)))
		(at end (n_basetodispose ?cs))

 	)
)



(:durative-action deliverProductToDScriticalTask
	:parameters (?r - robot ?ds - delivery_station ?p - product)
	:duration (= ?duration  20 ) 
	:condition (and  
		(at start (at ?r ?ds))
		(at start (free ?r))
		(at start (holding ?r ?p))	
		(at start (todeliver ?p))
		(at start (holdafter ?p))
	)
	:effect (and
		(at start (not (free ?r)))
		(at end (free ?r))
		(at end (n_holding ?r))
		(at end (not (holding ?r ?p)))
		(at end (delivered ?p ))
		(at end (not (todeliver ?p)))
	)
)






)
