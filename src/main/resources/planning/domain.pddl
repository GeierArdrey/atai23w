(define (domain rcll)
  (:requirements :strips :typing :durative-actions :numeric-fluents)
  
  ; Define types
  (:types
    robot - object
    machine - object
    machine-type - object
    machine-state - object
    machine-side - location
    base-color - object
    cap-color - object
    ring-color - object
    order-complexity - object
    order-id - object
    ring-num - object
    location - object
    robot-state - object
    workpiece - object
    )

  (:constants
		BS CS DS RS - machine-type
		IDLE INPUT_RDY OUTPUT_RDY BUFFERED_OUTPUT_RDY - machine-state
		INPUT OUTPUT - machine-side
		BASE_RED BASE_BLACK - base-color
		CAP_BLACK CAP_GREY - cap-color
		RING_GREEN RING_YELLOW RING_BLUE RING_ORANGE - ring-color
	)

  ; Define predicates
  (:predicates
    (robot-at ?r - robot ?m - machine ?side - machine-side); Gives position of the robot
    (machine-side-free ?m - machine ?side - machine-side) ; Tells wether a side (INPUT/OUTPUT) of a machine is free
    (machine-in-state ?m - machine ?state - machine-state) ; Tells in which state a machine is
    (machine-has-type ?m - machine ?t -  machine-type) ; Tells is a machine has a given type
    (rs-machine-has-color ?m - machine ?ringcolor - ring-color) ; Tells wether a rs can offer a color
    (cs-machine-has-color ?m - machine ?capcolor - cap-color) ; Tells wether a cs can offer a color
    (cs-buffered-cap ?m - machine ?capcolor - cap-color) ;store information which cap-color is buffered in which machine
    (cs-is-unbuffered ?m - machine) ;store information if a cap is buffgered to prevent two buffer actions after another
    (robot-can-carry ?r - robot) ;Tells wether the robot carries something
    (robot-carries ?r - robot ?wp - workpiece) ;Tells wether the robot carries a given workpiece
    (robot-carries-uncapped-base ?r - robot) ;Tells wether a robot carries a "uncapped-workpiece" (base after removing cap in cs)

    (wp-at ?wp - workpiece ?m - machine ?side - machine-side) ; Gives location of wp
    (wp-no-rings-mounted ?wp - workpiece) ; Tells wether any ring was mounted at some point, required to use a base as payment
    (wp-no-cap-mounted ?wp - workpiece) ; Tells wether a cap was mounted, required to prevent a ring mounted afterwards (and thus keeps order)
    (wp-cap-mounted ?wp - workpiece) ; counter-positive of above...
    (wp-base-mounted ?wp - workpiece) ; Tells wether a base was mounted, which is true after it was dispensed
    (wp-ring1-mounted ?wp - workpiece) ;Tells wether a ring is mounted
    (wp-ring2-mounted ?wp - workpiece)
    (wp-ring3-mounted ?wp - workpiece)
    (wp-base-color ?wp - workpiece ?basecolor - base-color) ;Keep track of mounted colors
    (wp-ring1-color ?wp - workpiece ?ringcolor - ring-color)
    (wp-ring2-color ?wp - workpiece ?ringcolor - ring-color)
    (wp-ring3-color ?wp - workpiece ?ringcolor - ring-color)
    (wp-cap-color ?wp - workpiece ?capcolor - cap-color)
     
    (wp-finished ?wp - workpiece) ;actually not requiered, might use it later

    ; Used to finish orders in different complexities
    (c0-finished ?orderid - order-id)
    (c1-finished ?orderid - order-id)
    (c2-finished ?orderid - order-id)
    (c3-finished ?orderid - order-id)

    ; Keeps track order-id and appropiate colors. Required to specify a order in problem file
    (order-base-color ?orderid - order-id ?basecolor - base-color)
    (order-ring1-color ?orderid - order-id ?ringcolor - ring-color)
    (order-ring2-color ?orderid - order-id ?ringcolor - ring-color)
    (order-ring3-color ?orderid - order-id ?ringcolor - ring-color)
    (order-cap-color ?orderid - order-id ?capcolor - cap-color)
  )

  (:functions
    (path-length ?from_m - machine ?from_s - machine-side ?to_m - machine ?to_s - machine-side)
    (bases-loaded ?m - machine) ; This represents the number of bases currently loaded
    (bases-required ?ringcolor - ring-color)
    (current-time) ; Tracks the current time
    (deadline ?orderid - order-id) ; Deadline for each workpiece, normally used in ds-finish* acrtion (at bottom). Disabled for performance  
  )
  
  ; Generally, the move is split up into 4 smaller steps. A Robot can move the following ways, with the intention of performance improvements
  ; Moving takes time proportional to the path length (which is 4 for every path for simplicity)
  ; 1. Move from any machine to an ouput of a different machine -> move-to-output
  ; 2. Move from any machine to an input of any different machine -> move-to-input
  ; 3. Move from the input of a machine to the output of the same machine -> move-to-output-same
  ; 4. Move from an output of a machine to the input of the same machine -> move-ro-output-same

  ; Moves the robot to an output of a different machine
  (:durative-action move-to-output
    :parameters (?r - robot ?from_m - machine ?from_s - machine-side ?to_m - machine)
    :duration (= ?duration 4)
    :condition (and (at start (robot-at ?r ?from_m ?from_s))
                    (at start (machine-side-free ?to_m OUTPUT)))
    :effect (and (at start  (not (robot-at ?r ?from_m ?from_s)))
                (at end  (not (machine-side-free ?to_m OUTPUT)))
                (at end (robot-at ?r ?to_m OUTPUT))
                (at start  (machine-side-free ?from_m ?from_s))
                (at end(increase (current-time) (path-length ?from_m ?from_s ?to_m OUTPUT))))
  )

  ; Moves the robot from the input of a machine to the output of the same machine
  (:durative-action move-to-output-same
    :parameters (?r - robot ?from_m - machine ?from_s - machine-side)
    :duration (= ?duration (path-length ?from_m ?from_s ?from_m OUTPUT))
    :condition (and (at start (robot-at ?r ?from_m ?from_s))
                    (at start (machine-side-free ?from_m OUTPUT)))
    :effect (and (at start  (not (robot-at ?r ?from_m ?from_s)))
                (at end  (not (machine-side-free ?from_m OUTPUT)))
                (at end (robot-at ?r ?from_m OUTPUT))
                (at start  (machine-side-free ?from_m ?from_s))
                (at end(increase (current-time) (path-length ?from_m ?from_s ?from_m OUTPUT))))
  )


  ; Moves the robot from any machine and side to the input of any other machine
  (:durative-action move-to-input
    :parameters (?r - robot ?from_m - machine ?from_s - machine-side ?to_m - machine)
    :duration (= ?duration (path-length ?from_m ?from_s ?to_m INPUT))
    :condition (and (at start (robot-at ?r ?from_m ?from_s))
                    (at start (machine-side-free ?to_m INPUT)))
    :effect (and  (at start  (not (robot-at ?r ?from_m ?from_s)))
                  (at end (not (machine-side-free ?to_m INPUT)))
                  (at end (robot-at ?r ?to_m INPUT))
                  (at start  (machine-side-free ?from_m ?from_s))
                  (at end(increase (current-time) (path-length ?from_m ?from_s ?to_m INPUT))))
  )


  ; Moves the robot from the output to the input of the same machine
  (:durative-action move-to-input-same
    :parameters (?r - robot ?from_m - machine ?from_s - machine-side)
    :duration (= ?duration (path-length ?from_m ?from_s ?from_m INPUT))
    :condition (and (at start (robot-at ?r ?from_m ?from_s))
                    (at start (machine-side-free ?from_m INPUT)))
    :effect (and  (at start  (not (robot-at ?r ?from_m ?from_s)))
                  (at end (not (machine-side-free ?from_m INPUT)))
                  (at end (robot-at ?r ?from_m INPUT))
                  (at start  (machine-side-free ?from_m ?from_s))
                  (at end (increase (current-time) (path-length ?from_m ?from_s ?from_m INPUT))))
  )

  ; A WP located at a machine output is picked up by the robot
  ; Assuemd to take a constant amount of time
  (:durative-action get-wp
    :parameters (?r - robot ?m - machine ?wp - workpiece)
    :duration (= ?duration 1)
    :condition (and (over all (robot-at ?r ?m OUTPUT))
                    (at start (robot-can-carry ?r))
                    (at start (machine-in-state ?m OUTPUT_RDY))
                    (at start (wp-at ?wp ?m OUTPUT)))
    :effect (and (at end (not (robot-can-carry ?r)))
                  (at end (robot-carries ?r ?wp))
                  (at start (not(wp-at ?wp ?m OUTPUT)))
                  (at start (not (machine-in-state ?m OUTPUT_RDY)))
                  (at end (machine-in-state ?m IDLE))
                  (at end(increase (current-time) 1)))
  )

  ; A WP is put from the robot to the INPUT of a machine
  ; Assuemd to take a constant amount of time
  (:durative-action put-wp
    :parameters (?r - robot ?m - machine ?wp - workpiece)
    :duration (= ?duration 1)
    :condition (and 
                  (over all (robot-at ?r ?m INPUT)) 
                  (at start (robot-carries ?r ?wp))
                  (at start (machine-in-state ?m IDLE)))
    :effect (and (at end (robot-can-carry ?r))
                  (at start (not (robot-carries ?r ?wp)))
                  (at end (wp-at ?wp ?m INPUT))
                  (at start (not (machine-in-state ?m IDLE)))
                  (at end (machine-in-state ?m INPUT_RDY))
                  (at end(increase (current-time) 1)))
   )

  ; Buffer a CS by taking a base+cap from SS and out it in the CS
  ; note SS is not explicitly modeled - e.g. number of shelf spaces etc.
  ; Assuemd to take a constant amount of time
  (:durative-action cs-buffer-cap
  :parameters (?r - robot ?m - machine ?wp - workpiece ?capcolor - cap-color ?orderid - order-id)
  :duration (= ?duration 1)
  :condition (and (over all (robot-at ?r ?m INPUT))
                  (at start (cs-is-unbuffered ?m))
                  (at start (robot-can-carry ?r))
                  (over all (machine-has-type ?m CS))
                  (at start (machine-in-state ?m IDLE))
                  (over all (order-cap-color ?orderid ?capcolor))
                  (over all (cs-machine-has-color ?m ?capcolor)))
  :effect (and (at end (cs-buffered-cap ?m ?capcolor))
                (at end (not (cs-is-unbuffered ?m)))
                (at start (not (machine-in-state ?m IDLE)))
                (at end (machine-in-state ?m BUFFERED_OUTPUT_RDY))
                (at end(increase (current-time) 1)))
  )  

  ; A base without cap "uncapped" is picked up from a CS, after it was buffered
  ; Assuemd to take a constant amount of time
  (:durative-action get-uncapped-base
    :parameters (?r - robot ?m - machine)
    :duration (= ?duration 1)
    :condition (and (over all (robot-at ?r ?m OUTPUT))
                    (over all (machine-has-type ?m CS))
                    (at start (robot-can-carry ?r))
                    (at start (machine-in-state ?m BUFFERED_OUTPUT_RDY)))
    :effect (and (at end (not (robot-can-carry ?r)))
                  (at start (not (machine-in-state ?m BUFFERED_OUTPUT_RDY)))
                  (at end (machine-in-state ?m IDLE))
                  (at end (robot-carries-uncapped-base ?r))
                  (at end(increase (current-time) 1)))
  )

  ; Discard an uncapped-base at a delivery station
  ; Assuemd to take a constant amount of time
  ; ...alternativly pay a ring station -> se below
  (:durative-action put-uncapped-base-at-ds
    :parameters (?r - robot ?m - machine)
    :duration (= ?duration 1)
    :condition (and (over all (robot-at ?r ?m INPUT))
                    (at start (robot-carries-uncapped-base ?r))
                    (over all (machine-has-type ?m DS)))
    :effect   (and (at end (and (robot-can-carry ?r)))
                  (at end (not (robot-carries-uncapped-base ?r)))
                  (at end(increase (current-time) 1)))
  )

  ; Deliver an uncapped-base at a ring station as a payment (Slide assumed to be at INPUT)
  ; Assuemd to take a constant amount of time
  (:durative-action put-uncapped-base-to-rs
    :parameters (?r - robot ?m - machine)
    :duration (= ?duration 1)
    :condition (and (over all (robot-at ?r ?m INPUT))
                    (at start (robot-carries-uncapped-base ?r))
                    (over all (machine-has-type ?m RS))
                    (at start (<= (bases-loaded ?m) 3)))
    :effect (and  (at end (robot-can-carry ?r))
                  (at end (not (robot-carries-uncapped-base ?r)))
                  (at end (increase (bases-loaded ?m) 1))
                  (at end(increase (current-time) 1)))
   )
  
  ; Puts a base aquired from the BS at a RS slide (Slide assumed to be at INPUT)
  (:durative-action put-normal-base-to-rs
    :parameters (?r - robot ?m - machine ?wp - workpiece)
    :duration (= ?duration 1)
    :condition (and (over all (robot-at ?r ?m INPUT))
                      (at start (robot-carries ?r ?wp))
                      (at start (wp-no-rings-mounted ?wp))
                      (at start (wp-no-cap-mounted ?wp))
                      (over all (machine-has-type ?m RS))
                      (at start (<= (bases-loaded ?m) 3)))
    :effect (and (at end (robot-can-carry ?r))
                (at end (not (robot-carries ?r ?wp)))
                (at end (increase (bases-loaded ?m) 1))
                (at end(increase (current-time) 1)))
  )
  
  ;;;;;;
  ; The following are Machine signals
  ; It will trigger an action, which is assumed to be instantanious, as suggested in the lecture
  ; (a machine action will always take place after the robot has moved as thus is included in this time)
  ;;;;;;

  ; Signal to BS to output a base meant to be assembled
  (:action bs-base-dispense
  :parameters (?m - machine  ?wp - workpiece ?basecolor - base-color ?order - order-id)
  :precondition (and (machine-has-type ?m BS)
                  (machine-in-state ?m IDLE)
                  (wp-base-color ?wp ?basecolor)
                  (order-base-color ?order ?basecolor)                  
                  (wp-at ?wp ?m INPUT));meaning stored here
  :effect (and  (wp-base-mounted ?wp)
                (wp-no-rings-mounted ?wp)
                (wp-no-cap-mounted ?wp)
                (wp-base-color ?wp ?basecolor) ;
                (not (machine-in-state ?m IDLE))
                (machine-in-state ?m OUTPUT_RDY)
                (not (wp-at ?wp ?m INPUT))
                (wp-at ?wp ?m OUTPUT))
  )

  ; Signal to bs to output a base meant for paying a RS
  (:action bs-base-dispense-for-paying
  :parameters (?m - machine  ?wp - workpiece ?basecolor - base-color)
  :precondition (and (machine-has-type ?m BS)
                  (machine-in-state ?m IDLE)
                  (wp-base-color ?wp ?basecolor)
                  (wp-at ?wp ?m INPUT));meaning stored here
  :effect (and  (not (machine-in-state ?m IDLE))
                (wp-no-rings-mounted ?wp)
                (wp-no-cap-mounted ?wp)
                (machine-in-state ?m OUTPUT_RDY)
                (not (wp-at ?wp ?m INPUT))
                (wp-at ?wp ?m OUTPUT))
  )
  

  ; Mounts the first ring to a workpiece
  (:action rs-mount-ring1
  :parameters (?m - machine ?wp - workpiece ?order - order-id ?ringcolor - ring-color)
  :precondition (and (machine-has-type ?m RS)
                  (machine-in-state ?m INPUT_RDY)
                  (wp-at ?wp ?m INPUT)
                  (order-ring1-color ?order ?ringcolor)
                  (rs-machine-has-color ?m ?ringcolor)
                  (wp-base-mounted ?wp)
                  (wp-no-cap-mounted ?wp)
                  (>= (bases-loaded ?m) (bases-required ?ringcolor)))             
  :effect (and (wp-ring1-mounted ?wp)
                (wp-ring1-color ?wp ?ringcolor)
                (not (wp-no-rings-mounted ?wp))
                (not (machine-in-state ?m INPUT_RDY))
                (machine-in-state ?m OUTPUT_RDY)
                (not (wp-at ?wp ?m INPUT))
                (wp-at ?wp ?m OUTPUT)
                (decrease (bases-loaded ?m) (bases-required ?ringcolor)))            
  )

  ; Mounts the second ring to a workpiece
  (:action rs-mount-ring2
  :parameters (?m - machine ?wp - workpiece ?order - order-id ?ringcolor - ring-color)
  :precondition (and (machine-has-type ?m RS)
                  (machine-in-state ?m INPUT_RDY)
                  (wp-at ?wp ?m INPUT)
                  (order-ring2-color ?order ?ringcolor)
                  (rs-machine-has-color ?m ?ringcolor)
                  (wp-base-mounted ?wp)
                  (wp-ring1-mounted ?wp)
                  (wp-no-cap-mounted ?wp)
                  (>= (bases-loaded ?m) (bases-required ?ringcolor)))  
  :effect (and (wp-ring2-mounted ?wp)
                (wp-ring2-color ?wp ?ringcolor)
                (not (machine-in-state ?m INPUT_RDY))
                (machine-in-state ?m OUTPUT_RDY)
                (not (wp-at ?wp ?m INPUT))
                (wp-at ?wp ?m OUTPUT)
                (decrease (bases-loaded ?m) (bases-required ?ringcolor)))                
  )

  ; Mounts the third ring to a workpiece
  (:action rs-mount-ring3
  :parameters (?m - machine ?wp - workpiece ?order - order-id ?ringcolor - ring-color)
  :precondition (and (machine-has-type ?m RS)
                  (machine-in-state ?m INPUT_RDY)
                  (wp-at ?wp ?m INPUT)
                  (order-ring3-color ?order ?ringcolor)
                  (rs-machine-has-color ?m ?ringcolor)
                  (wp-base-mounted ?wp)
                  (wp-ring1-mounted ?wp)
                  (wp-ring2-mounted ?wp)
                  (wp-no-cap-mounted ?wp)
                  (>= (bases-loaded ?m) (bases-required ?ringcolor)))  
  :effect (and (wp-ring3-mounted ?wp)
                (wp-ring3-color ?wp ?ringcolor)
                (not (machine-in-state ?m INPUT_RDY))
                (machine-in-state ?m OUTPUT_RDY)
                (not (wp-at ?wp ?m INPUT))
                (wp-at ?wp ?m OUTPUT)
                (decrease (bases-loaded ?m) (bases-required ?ringcolor)))               
  )

  ; Mounts a cap on a workpiece
  (:action cs-mount-cap
  :parameters (?m - machine ?wp - workpiece ?order - order-id ?capcolor - cap-color)
  :precondition (and (machine-has-type ?m CS)
                  (machine-in-state ?m INPUT_RDY)
                  (wp-no-cap-mounted ?wp)
                  (wp-at ?wp ?m INPUT)
                  (cs-machine-has-color ?m ?capcolor)
                  (cs-buffered-cap ?m ?capcolor)
                  (order-cap-color ?order ?capcolor)
                  (wp-base-mounted ?wp))
  :effect (and (wp-cap-mounted ?wp)
                (wp-cap-color ?wp ?capcolor)
                (not (wp-no-cap-mounted ?wp))
                (not (cs-buffered-cap ?m ?capcolor))
                (cs-is-unbuffered ?m)
                (not (machine-in-state ?m INPUT_RDY))
                (machine-in-state ?m OUTPUT_RDY)
                (not (wp-at ?wp ?m INPUT))
                (wp-at ?wp ?m OUTPUT))
  )


  ;;;;
  ; Orders are finished here (as an action from the DS)
  ; Deadlines are disabled for performance.... :(
  ;;;;
  (:action ds-finish-c0-order
    :parameters (?m - machine  ?wp - workpiece  ?orderid - order-id ?basecolor - base-color ?capcolor - cap-color)
    :precondition (and (machine-has-type ?m DS)    
                      ;(<= (deadline ?orderid)(current-time))                    
                      (wp-at ?wp ?m INPUT)                                        
                      (wp-base-mounted ?wp)(wp-base-color ?wp ?basecolor)(order-base-color ?orderid ?basecolor)
                      (wp-cap-mounted ?wp)(wp-cap-color ?wp ?capcolor)(order-cap-color ?orderid ?capcolor))
    :effect (and 
              (wp-finished ?wp)
              (c0-finished ?orderid))
  )


  (:action ds-finish-c1-order
    :parameters (?m - machine  ?wp - workpiece  ?orderid - order-id ?basecolor - base-color ?ring1color - ring-color ?capcolor - cap-color)
    :precondition (and (machine-has-type ?m DS)  
                      ;(<= (deadline ?orderid)(current-time))  
                      (wp-at ?wp ?m INPUT)                                        
                      (wp-base-mounted ?wp)(wp-base-color ?wp ?basecolor)(order-base-color ?orderid ?basecolor)
                      (wp-ring1-mounted ?wp)(wp-ring1-color ?wp ?ring1color)(order-ring1-color ?orderid ?ring1color)
                      (wp-cap-mounted ?wp)(wp-cap-color ?wp ?capcolor)(order-cap-color ?orderid ?capcolor))
    :effect (and 
              (wp-finished ?wp)
              (c1-finished ?orderid))
  )

  (:action ds-finish-c2-order
  :parameters (?m - machine  ?wp - workpiece  ?orderid - order-id ?basecolor - base-color ?ring1color - ring-color ?ring2color - ring-color ?capcolor - cap-color)
  :precondition (and (machine-has-type ?m DS)  
                    ;(<= (deadline ?orderid)(current-time))  
                    (wp-at ?wp ?m INPUT)                                        
                    (wp-base-mounted ?wp)(wp-base-color ?wp ?basecolor)(order-base-color ?orderid ?basecolor)
                    (wp-ring1-mounted ?wp)(wp-ring1-color ?wp ?ring1color)(order-ring1-color ?orderid ?ring1color)
                    (wp-ring2-mounted ?wp)(wp-ring2-color ?wp ?ring2color)(order-ring2-color ?orderid ?ring2color)
                    (wp-cap-mounted ?wp)(wp-cap-color ?wp ?capcolor))
  :effect (and 
            (wp-finished ?wp)
            (c2-finished ?orderid))
  )

  (:action ds-finish-c3-order
  :parameters (?m - machine  ?wp - workpiece  ?orderid - order-id ?basecolor - base-color ?ring1color - ring-color ?ring2color - ring-color ?ring3color - ring-color ?capcolor - cap-color)
  :precondition (and (machine-has-type ?m DS)  
                    ;(<= (deadline ?orderid)(current-time))  
                    (wp-at ?wp ?m INPUT)                                        
                    (wp-base-mounted ?wp)(wp-base-color ?wp ?basecolor)(order-base-color ?orderid ?basecolor)
                    (wp-ring1-mounted ?wp)(wp-ring1-color ?wp ?ring1color)(order-ring1-color ?orderid ?ring1color)
                    (wp-ring2-mounted ?wp)(wp-ring2-color ?wp ?ring2color)(order-ring2-color ?orderid ?ring2color)
                    (wp-ring3-mounted ?wp)(wp-ring3-color ?wp ?ring3color)(order-ring3-color ?orderid ?ring3color)
                    (wp-cap-mounted ?wp)(wp-cap-color ?wp ?capcolor)(order-cap-color ?orderid ?capcolor))
  :effect (and 
            (wp-finished ?wp)
            (c3-finished ?orderid))
  )


)


