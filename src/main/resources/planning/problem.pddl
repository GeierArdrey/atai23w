(define (problem rcll-problem1)
	(:domain rcll)
    
  (:objects
    robot1 robot2 - robot
    order1 order2 - order-id
    wp1 wp2 wp3 wp4 wp5 - workpiece
    DS1 BS1 CS1 CS2 RS1 RS2 START - machine ;START is the start location
  )
   
  (:init

    ;Defining all 6 Machines
    ;Typically, every Machine has a INPUT/OUTPUT side which can be occupied
    ;Every Machine has a state to reflect whats going on and prevent unallowed usage by robots, i.e. when something is ready at an output
    ;Every machine has a type to specify what it can actually do
    (machine-side-free DS1 INPUT) 
    ;(machine-side-free DS1 OUTPUT) machine has only one side
    (machine-in-state DS1 IDLE)
    (machine-has-type DS1 DS)

    (machine-side-free BS1 OUTPUT)
    (machine-in-state BS1 IDLE)
    (machine-has-type BS1 BS)

    (machine-side-free CS1 INPUT)
    (machine-side-free CS1 OUTPUT)
    (machine-in-state CS1 IDLE)
    (cs-machine-has-color CS1 CAP_BLACK) ;Machine can offer black caps
    (machine-has-type CS1 CS)
    (cs-is-unbuffered CS1) ;This represents the presence of a ring buffered in the machine

    (machine-side-free CS2 INPUT)
    (machine-side-free CS2 OUTPUT)
    (machine-in-state CS2 IDLE)
    (cs-machine-has-color CS2 CAP_GREY) ;Machine can offer grey caps
    (machine-has-type CS2 CS)
    (cs-is-unbuffered CS2) ;This represents the presence of a ring buffered in the machine

    (machine-side-free RS1 INPUT)
    (machine-side-free RS1 OUTPUT)
    (machine-in-state RS1 IDLE)
    (rs-machine-has-color RS1 RING_ORANGE)
    (rs-machine-has-color RS1 RING_YELLOW)
    (machine-has-type RS1 RS)
    (= (bases-loaded RS1) 0) ;This numeric value initialized bases on the slide

    (machine-side-free RS2 INPUT)
    (machine-side-free RS2 OUTPUT)
    (machine-in-state RS2 IDLE)
    (rs-machine-has-color RS2 RING_BLUE)
    (rs-machine-has-color RS2 RING_GREEN)
    (machine-has-type RS2 RS)
    (= (bases-loaded RS2) 0) ;This numeric value initialized bases on the slide

   
    ; Define color complexety prize
    ; In this example, every color has a prize of one. So whenever a ring is mounted it costs one base. 
    ; I wasnt shure about my interpretation of the rulebook, it says on page 14:
    ; "The robot may have to deliver one or more additional bases to a RS
    ; first in order to get a ring. We therefore distinguish color complexities as CC0, CC1, and CC2 depending on whether zero, one, or two additional bases are required for a color. The referee box ensures
    ; that there will always be some orders for C1 products where the ring color does not require additional
    ; bases, i.e., with color complexity CC0 — but not necessarily all C1 orders have this constraint."
    (= (bases-required RING_GREEN) 1)
    (= (bases-required RING_YELLOW) 1)
    (= (bases-required RING_BLUE) 1)
    (= (bases-required RING_ORANGE) 1)

    (= (current-time) 0) ; init clock
    
    ;Defining path length as a cost measure for time required to do move-actions
    ;Normally define all the distance adequatly.... skipped this for now
    ;I used a script to generate these, might do it for exercise 3 aswell with real distances
    (= (path-length BS1 INPUT BS1 OUTPUT) 4)
    (= (path-length BS1 OUTPUT BS1 INPUT) 4)
    (= (path-length BS1 INPUT RS1 OUTPUT) 4)
    (= (path-length BS1 OUTPUT RS1 INPUT) 4)
    (= (path-length BS1 INPUT RS2 OUTPUT) 4)
    (= (path-length BS1 OUTPUT RS2 INPUT) 4)
    (= (path-length BS1 INPUT CS1 OUTPUT) 4)
    (= (path-length BS1 OUTPUT CS1 INPUT) 4)
    (= (path-length BS1 INPUT CS2 OUTPUT) 4)
    (= (path-length BS1 OUTPUT CS2 INPUT) 4)
    (= (path-length BS1 INPUT DS1 OUTPUT) 4)
    (= (path-length BS1 OUTPUT DS1 INPUT) 4)
    (= (path-length BS1 INPUT START OUTPUT) 4)
    (= (path-length BS1 OUTPUT START INPUT) 4)
    (= (path-length RS1 INPUT BS1 OUTPUT) 4)
    (= (path-length RS1 OUTPUT BS1 INPUT) 4)
    (= (path-length RS1 INPUT RS1 OUTPUT) 4)
    (= (path-length RS1 OUTPUT RS1 INPUT) 4)
    (= (path-length RS1 INPUT RS2 OUTPUT) 4)
    (= (path-length RS1 OUTPUT RS2 INPUT) 4)
    (= (path-length RS1 INPUT CS1 OUTPUT) 4)
    (= (path-length RS1 OUTPUT CS1 INPUT) 4)
    (= (path-length RS1 INPUT CS2 OUTPUT) 4)
    (= (path-length RS1 OUTPUT CS2 INPUT) 4)
    (= (path-length RS1 INPUT DS1 OUTPUT) 4)
    (= (path-length RS1 OUTPUT DS1 INPUT) 4)
    (= (path-length RS1 INPUT START OUTPUT) 4)
    (= (path-length RS1 OUTPUT START INPUT) 4)
    (= (path-length RS2 INPUT BS1 OUTPUT) 4)
    (= (path-length RS2 OUTPUT BS1 INPUT) 4)
    (= (path-length RS2 INPUT RS1 OUTPUT) 4)
    (= (path-length RS2 OUTPUT RS1 INPUT) 4)
    (= (path-length RS2 INPUT RS2 OUTPUT) 4)
    (= (path-length RS2 OUTPUT RS2 INPUT) 4)
    (= (path-length RS2 INPUT CS1 OUTPUT) 4)
    (= (path-length RS2 OUTPUT CS1 INPUT) 4)
    (= (path-length RS2 INPUT CS2 OUTPUT) 4)
    (= (path-length RS2 OUTPUT CS2 INPUT) 4)
    (= (path-length RS2 INPUT DS1 OUTPUT) 4)
    (= (path-length RS2 OUTPUT DS1 INPUT) 4)
    (= (path-length RS2 INPUT START OUTPUT) 4)
    (= (path-length RS2 OUTPUT START INPUT) 4)
    (= (path-length CS1 INPUT BS1 OUTPUT) 4)
    (= (path-length CS1 OUTPUT BS1 INPUT) 4)
    (= (path-length CS1 INPUT RS1 OUTPUT) 4)
    (= (path-length CS1 OUTPUT RS1 INPUT) 4)
    (= (path-length CS1 INPUT RS2 OUTPUT) 4)
    (= (path-length CS1 OUTPUT RS2 INPUT) 4)
    (= (path-length CS1 INPUT CS1 OUTPUT) 4)
    (= (path-length CS1 OUTPUT CS1 INPUT) 4)
    (= (path-length CS1 INPUT CS2 OUTPUT) 4)
    (= (path-length CS1 OUTPUT CS2 INPUT) 4)
    (= (path-length CS1 INPUT DS1 OUTPUT) 4)
    (= (path-length CS1 OUTPUT DS1 INPUT) 4)
    (= (path-length CS1 INPUT START OUTPUT) 4)
    (= (path-length CS1 OUTPUT START INPUT) 4)
    (= (path-length CS2 INPUT BS1 OUTPUT) 4)
    (= (path-length CS2 OUTPUT BS1 INPUT) 4)
    (= (path-length CS2 INPUT RS1 OUTPUT) 4)
    (= (path-length CS2 OUTPUT RS1 INPUT) 4)
    (= (path-length CS2 INPUT RS2 OUTPUT) 4)
    (= (path-length CS2 OUTPUT RS2 INPUT) 4)
    (= (path-length CS2 INPUT CS1 OUTPUT) 4)
    (= (path-length CS2 OUTPUT CS1 INPUT) 4)
    (= (path-length CS2 INPUT CS2 OUTPUT) 4)
    (= (path-length CS2 OUTPUT CS2 INPUT) 4)
    (= (path-length CS2 INPUT DS1 OUTPUT) 4)
    (= (path-length CS2 OUTPUT DS1 INPUT) 4)
    (= (path-length CS2 INPUT START OUTPUT) 4)
    (= (path-length CS2 OUTPUT START INPUT) 4)
    (= (path-length DS1 INPUT BS1 OUTPUT) 4)
    (= (path-length DS1 OUTPUT BS1 INPUT) 4)
    (= (path-length DS1 INPUT RS1 OUTPUT) 4)
    (= (path-length DS1 OUTPUT RS1 INPUT) 4)
    (= (path-length DS1 INPUT RS2 OUTPUT) 4)
    (= (path-length DS1 OUTPUT RS2 INPUT) 4)
    (= (path-length DS1 INPUT CS1 OUTPUT) 4)
    (= (path-length DS1 OUTPUT CS1 INPUT) 4)
    (= (path-length DS1 INPUT CS2 OUTPUT) 4)
    (= (path-length DS1 OUTPUT CS2 INPUT) 4)
    (= (path-length DS1 INPUT DS1 OUTPUT) 4)
    (= (path-length DS1 OUTPUT DS1 INPUT) 4)
    (= (path-length DS1 INPUT START OUTPUT) 4)
    (= (path-length DS1 OUTPUT START INPUT) 4)
    (= (path-length START INPUT BS1 OUTPUT) 4)
    (= (path-length START OUTPUT BS1 INPUT) 4)
    (= (path-length START INPUT RS1 OUTPUT) 4)
    (= (path-length START OUTPUT RS1 INPUT) 4)
    (= (path-length START INPUT RS2 OUTPUT) 4)
    (= (path-length START OUTPUT RS2 INPUT) 4)
    (= (path-length START INPUT CS1 OUTPUT) 4)
    (= (path-length START OUTPUT CS1 INPUT) 4)
    (= (path-length START INPUT CS2 OUTPUT) 4)
    (= (path-length START OUTPUT CS2 INPUT) 4)
    (= (path-length START INPUT DS1 OUTPUT) 4)
    (= (path-length START OUTPUT DS1 INPUT) 4)
    (= (path-length START INPUT START OUTPUT) 4)
    (= (path-length START OUTPUT START INPUT) 4)


    ;Robot starts at a "start machine, input side", which is just a location
    (robot-at robot1 START INPUT)
    (robot-can-carry robot1)
  
    ;
    ; Here comes the ORDER SETUP
    ; Here comes the ORDER SETUP
    ; Here comes the ORDER SETUP
    ; Here comes the ORDER SETUP
    ;

    ; Configure & Put base-elements at BS.
    ; For every base element you want to put, copy those 2 lines and add a wp instance at the very top:
    ; In my implementation, bases are dispesed from a bs and treated as a wp from that on
    (wp-base-color wp1 BASE_RED)
    (wp-at wp1 BS1 INPUT) ;loaded at BS1

    (wp-base-color wp2 BASE_RED)
    (wp-at wp1 BS1 INPUT) ;loaded at BS1

    (wp-base-color wp3 BASE_BLACK)
    (wp-at wp2 BS1 INPUT) ;loaded at BS1

    (wp-base-color wp4 BASE_BLACK)
    (wp-at wp3 BS1 INPUT) ;loaded at BS1

    (wp-base-color wp5 BASE_BLACK)
    (wp-at wp4 BS1 INPUT) ;loaded at BS1

    ;;;
    ; Specify everything about the orders here
    ; Requires of course that a fitting (color) base is loaded to BS (above)
    ; I disabled deadlines as they cause very long search. I also commented the deadline requirement in the domain at the bottom
    ;;;
    (= (deadline order1) 100) ; Deadline for order1
    (order-base-color order1 BASE_RED)
    (order-ring1-color order1 RING_BLUE)
    (order-ring2-color order1 RING_YELLOW)
    (order-ring3-color order1 RING_ORANGE)
    (order-cap-color order1 CAP_BLACK)

    ; (= (deadline order2) 150) ; Deadline for order2
    ; (order-base-color order2 BASE_BLACK)
    ; (order-ring1-color order2 RING_BLUE)
    ; (order-cap-color order2 CAP_GREY)

  )

  ; Specify the goal, which are orders and complexities.
  ; For every order there must be an instance like "order1" -> define at the top
  ; Recommended to first test single orders -> performance
  (:goal (and 
          ;(c0-finished order2)
          (c3-finished order1)
          )
  )
)



