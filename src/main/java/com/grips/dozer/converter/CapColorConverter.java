package com.grips.dozer.converter;

import com.rcll.domain.Cap;
import org.dozer.CustomConverter;
import org.robocup_logistics.llsf_msgs.ProductColorProtos;

public class CapColorConverter implements CustomConverter {
    @Override
    public Object convert(Object dest, Object src, Class<?> destinationClass, Class<?> sourceClass) {
        if (src == null) return null;

        if (src instanceof ProductColorProtos.CapColor) {
            ProductColorProtos.CapColor protoCap = (ProductColorProtos.CapColor) src;
            switch (protoCap) {
                case CAP_BLACK:
                    return Cap.Black;
                case CAP_GREY:
                    return Cap.Grey;
            }
        } else {
            throw new RuntimeException("Error Converting ProtoTime! " + dest + "/" + src);
        }
        return dest;
    }
}
