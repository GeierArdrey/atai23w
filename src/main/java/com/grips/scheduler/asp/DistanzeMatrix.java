package com.grips.scheduler.asp;


import com.grips.config.RefboxConfig;
import com.grips.config.StartPositionsConfig;
import com.grips.config.StationsConfig;
import com.rcll.domain.MachineSide;
import com.rcll.domain.TeamColor;
import com.grips.persistence.dao.MachineInfoRefBoxDao;
import com.grips.persistence.domain.MachineInfoRefBox;
import com.grips.tools.PathEstimator;
import com.rcll.refbox.RefboxClient;
import com.shared.domain.Point2d;
import com.shared.domain.Point2dInt;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
//todo not related to GRIPS, just calculates the time needed to travel from A to B.
@Service
@CommonsLog
public class DistanzeMatrix {
    private final RefboxClient refboxClient;
    private Map<String, Double> distanceMatrix;
    private final MachineInfoRefBoxDao machineInfoRefBoxDao;
    private final PathEstimator pathEstimator;
    private final RefboxConfig refboxConfig;

    private final StationsConfig stationsConfig;

    private final StartPositionsConfig startPositionsConfig;
    public DistanzeMatrix(MachineInfoRefBoxDao machineInfoRefBoxDao, PathEstimator pathEstimator,
                          StationsConfig stationsConfig, StartPositionsConfig startPositionsConfig,
                          RefboxConfig refboxConfig, RefboxClient refboxClient) {
        this.machineInfoRefBoxDao = machineInfoRefBoxDao;
        this.pathEstimator = pathEstimator;
        this.stationsConfig = stationsConfig;
        this.refboxConfig = refboxConfig;
        this.startPositionsConfig = startPositionsConfig;
        this.refboxClient = refboxClient;
    }

    //DISTANCE MATRIX CALCULATED USING A*
    public void generateDistanceMatrix() {
        distanceMatrix = new HashMap<>();
        List<MachineInfoRefBox> machinelist = machineInfoRefBoxDao.findByTeamColor(refboxClient.getTeamColor().orElseThrow());
        while (machinelist.size() < stationsConfig.countUsedMachines()) {
            log.info("Machine missing(having: " + machinelist.size() + " want " + stationsConfig.countUsedMachines() + "...waiting!");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        for (MachineInfoRefBox machine1 : machinelist) {
            for (MachineInfoRefBox machine2 : machinelist) {
                double distance;
                if (machine1.getType().equals("CS")) {
                    if (machine2.getType().equals("BS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Input, machine2.getName(), MachineSide.Output);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Shelf, machine2.getName(), MachineSide.Output);
                        distanceMatrix.put(machine1.getName() + "_shelf" + machine2.getName() + "_output", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Output, machine2.getName(), MachineSide.Output);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_output", distance);
                    } else if (machine2.getType().equals("CS")) {

                        if (!(machine1.getName().equals(machine2.getName()))) {
                            distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Input, machine2.getName(), MachineSide.Input);
                            distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_input", distance);
                        }
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Shelf, machine2.getName(), MachineSide.Input);
                        distanceMatrix.put(machine1.getName() + "_shelf" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Output, machine2.getName(), MachineSide.Input);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Input, machine2.getName(), MachineSide.Shelf);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_shelf", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Input, machine2.getName(), MachineSide.Output);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                        if (!(machine1.getName().equals(machine2.getName()))) {
                            distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Shelf, machine2.getName(), MachineSide.Shelf);
                            distanceMatrix.put(machine1.getName() + "_shelf" + machine2.getName() + "_shelf", distance);
                        }
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Shelf, machine2.getName(), MachineSide.Output);
                        distanceMatrix.put(machine1.getName() + "_shelf" + machine2.getName() + "_output", distance);

                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Output, machine2.getName(), MachineSide.Shelf);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_shelf", distance);
                        if (!(machine1.getName().equals(machine2.getName()))) {
                            distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Output, machine2.getName(), MachineSide.Output);
                            distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_output", distance);
                        }
                    } else if (machine2.getType().equals("RS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Input, machine2.getName(), MachineSide.Output);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Shelf, machine2.getName(), MachineSide.Output);
                        distanceMatrix.put(machine1.getName() + "_shelf" + machine2.getName() + "_output", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Output, machine2.getName(), MachineSide.Output);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_output", distance);

                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Input, machine2.getName(), MachineSide.Input);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Shelf, machine2.getName(), MachineSide.Input);
                        distanceMatrix.put(machine1.getName() + "_shelf" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Output, machine2.getName(), MachineSide.Input);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                    } else if (machine2.getType().equals("DS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Input, machine2.getName(), MachineSide.Input);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Shelf, machine2.getName(), MachineSide.Input);
                        distanceMatrix.put(machine1.getName() + "_shelf" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Output, machine2.getName(), MachineSide.Input);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                    }
                } else if (machine1.getType().equals("BS")) {
                    if (machine2.getType().equals("CS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Output, machine2.getName(), MachineSide.Input);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Output, machine2.getName(), MachineSide.Shelf);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_shelf", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Output, machine2.getName(), MachineSide.Output);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_output", distance);
                    } else if (machine2.getType().equals("RS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Output, machine2.getName(), MachineSide.Output);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_output", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Output, machine2.getName(), MachineSide.Input);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                    } else if (machine2.getType().equals("DS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Output, machine2.getName(), MachineSide.Input);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                    }
                } else if (machine1.getType().equals("RS")) {
                    if (machine2.getType().equals("BS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Input, machine2.getName(), MachineSide.Output);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Output, machine2.getName(), MachineSide.Output);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_output", distance);
                    } else if (machine2.getType().equals("CS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Input, machine2.getName(), MachineSide.Input);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Output, machine2.getName(), MachineSide.Input);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Input, machine2.getName(), MachineSide.Shelf);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_shelf", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Input, machine2.getName(), MachineSide.Output);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Output, machine2.getName(), MachineSide.Shelf);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_shelf", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Output, machine2.getName(), MachineSide.Output);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_output", distance);

                    } else if (machine2.getType().equals("RS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Input, machine2.getName(), MachineSide.Output);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                        if (!(machine1.getName().equals(machine2.getName()))) {
                            distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Output, machine2.getName(), MachineSide.Output);
                            distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_output", distance);
                            distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Input, machine2.getName(), MachineSide.Input);
                            distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_input", distance);
                        }
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Output, machine2.getName(), MachineSide.Input);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                    } else if (machine2.getType().equals("DS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Input, machine2.getName(), MachineSide.Input);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Output, machine2.getName(), MachineSide.Input);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                    }
                } else if (machine1.getType().equals("DS")) {
                    if (machine2.getType().equals("BS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Input, machine2.getName(), MachineSide.Output);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                    } else if (machine2.getType().equals("CS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Input, machine2.getName(), MachineSide.Input);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Input, machine2.getName(), MachineSide.Shelf);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_shelf", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Input, machine2.getName(), MachineSide.Output);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                    } else if (machine2.getType().equals("RS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Input, machine2.getName(), MachineSide.Output);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.Input, machine2.getName(), MachineSide.Input);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_input", distance);
                    }
                }
                //starting point 115/5
                //Point2dInt startingPoint = new Point2dInt(115, 5);
                Point2dInt startingPoint = pathEstimator.rcllToGrid(new Point2d(
                        startPositionsConfig.getCyan().getRobot1().getX(),
                        startPositionsConfig.getCyan().getRobot1().getY()));
                if (TeamColor.MAGENTA.equals(refboxClient.getTeamColor().orElseThrow())) {
                    startingPoint = pathEstimator.rcllToGrid(new Point2d(
                            startPositionsConfig.getMagenta().getRobot1().getX(),
                            startPositionsConfig.getMagenta().getRobot1().getY()));
                }
                double distancefromstart;
                if (machine1.getType().equals("DS")) {
                    distancefromstart = pathEstimator.pathLength(startingPoint, machine1.getName(), MachineSide.Input);
                    distanceMatrix.put("start" + machine1.getName() + "_input", distancefromstart);
                } else if (machine1.getType().equals("CS")) {
                    distancefromstart = pathEstimator.pathLength(startingPoint, machine1.getName(), MachineSide.Input);
                    distanceMatrix.put("start" + machine1.getName() + "_input", distancefromstart);
                    distancefromstart = pathEstimator.pathLength(startingPoint, machine1.getName(), MachineSide.Shelf);
                    distanceMatrix.put("start" + machine1.getName() + "_shelf", distancefromstart);
                    distancefromstart = pathEstimator.pathLength(startingPoint, machine1.getName(), MachineSide.Output);
                    distanceMatrix.put("start" + machine1.getName() + "_output", distancefromstart);
                } else if (machine1.getType().equals("RS")) {
                    distancefromstart = pathEstimator.pathLength(startingPoint, machine1.getName(), MachineSide.Input);
                    distanceMatrix.put("start" + machine1.getName() + "_input", distancefromstart);
                    distancefromstart = pathEstimator.pathLength(startingPoint, machine1.getName(), MachineSide.Output);
                    distanceMatrix.put("start" + machine1.getName() + "_output", distancefromstart);
                } else if (machine1.getType().equals("BS")) {
                    distancefromstart = pathEstimator.pathLength(startingPoint, machine1.getName(), MachineSide.Output);
                    distanceMatrix.put("start" + machine1.getName() +  "_output", distancefromstart);
                }
            }
        }
    }

    public Map<String, Double> getDistanzMatrix() {
        return this.distanceMatrix;
    }
}
