package com.grips.scheduler.asp;

import com.grips.config.ProductionTimesConfig;
import com.grips.persistence.dao.*;
import com.grips.persistence.domain.*;
import com.grips.robot.RobotClientWrapper;
import com.grips.scheduler.GameField;
import com.grips.scheduler.api.IScheduler;
import com.grips.scheduler.asp.dispatcher.ActionMapping;
import com.rcll.domain.*;
import com.rcll.planning.encoding.RcllCodedProblem;
import com.rcll.planning.encoding.IntOp;
import com.rcll.planning.planner.Planner;
import com.rcll.planning.planparser.TemporalEdge;
import com.rcll.planning.planparser.TemporalGraph;
import com.rcll.planning.planparser.TemporalNode;
import com.grips.scheduler.exploration.PreProductionService;
import com.rcll.refbox.RefboxClient;
import com.robot_communication.services.GripsRobotClient;
import lombok.SneakyThrows;
import lombok.extern.apachecommons.CommonsLog;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.robocup_logistics.llsf_msgs.AgentTasksProtos;
import org.robocup_logistics.llsf_msgs.BeaconSignalProtos;
import org.robocup_logistics.llsf_msgs.MachineInfoProtos;
import org.robocup_logistics.llsf_msgs.TeamProtos;
import org.springframework.beans.factory.annotation.Value;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

import static com.grips.persistence.domain.SubProductionTask.TaskState.*;

@CommonsLog
public class AspScheduler implements IScheduler {
    private final SubProductionTaskDao subProductionTaskDao;
    private final GripsRobotClient robotClient;

    private final WorldKnowledge worldKnowledge;

    private final RefboxClient refboxClient;
    private final BeaconSignalFromRobotDao beaconSignalFromRobotDao;
    private final RobotClientWrapper robotClientWrapper;
    private final RobotStartChecker robotStartChecker;

    private final double minValue = 0.001;
    private PlanningThread winningThread;

    @Value("${gameconfig.planningparameters.minorderstoplan}")
    private int minOrdersToPlan;
    @Value("${gameconfig.planningparameters.productiontime}")
    private int productiontime;

    @Value("${gameconfig.planningparameters.reset_on_start}")
    private boolean resetOnStart;

    @Value("${gameconfig.planningparameters.maxreplanningtimedl}")
    private int maxReplanDl;
    @Value("${gameconfig.planningparameters.leavestartingarea}")
    private boolean leaveStartingZoneTask;
    private final DistanzeMatrix distanzeMatrix;

    private boolean first = true;
    private boolean firstR2 = true;
    private boolean firstR3 = true;
    RcllCodedProblem cp;
    private List<Order> goals;
    private boolean stop;

    private boolean robotStopped[];
    private int replanRequest[];
    static long timerStarting[];

    private final TemporalNode[] executing;
    private final boolean[] cleanTask;

    private final boolean[] makeCleanTaskFail;
    private TemporalGraph tgraph;
    private final KnowledgeBase kb;
    private final ActionMapping actionMapping;
    private Boolean starting;
    private int actuaReplanDl = 0;
    private long lastReplanTime = 0;

    private final GameField gameField;
    private final boolean threadDebug = false;

    private final MachineInfoRefBoxDao machineInfoRefBoxDao;
    private final ProductionTimesConfig productionTimesConfig;
    private long initalWaitingTaskId;

    public AspScheduler(GripsRobotClient robotClient, RefboxClient refboxClient,
                        SubProductionTaskDao subProductionTaskDao, BeaconSignalFromRobotDao beaconSignalFromRobotDao,
                        RobotClientWrapper robotClientWrapper, PreProductionService preProductionService,
                        DistanzeMatrix distanzeMatrix, KnowledgeBase kb,
                        RobotStartChecker robotStartChecker, ActionMapping actionMapping, GameField gameField,
                        ProductionTimesConfig productionTimesConfig, MachineInfoRefBoxDao machineInfoRefBoxDao) {
        this.kb = kb;
        this.robotClient = robotClient;
        this.refboxClient = refboxClient;
        this.subProductionTaskDao = subProductionTaskDao;
        this.beaconSignalFromRobotDao = beaconSignalFromRobotDao;
        this.distanzeMatrix = distanzeMatrix;
        this.robotStartChecker = robotStartChecker;
        this.actionMapping = actionMapping;
        this.gameField = gameField;
        cleanTask = new boolean[3];
        makeCleanTaskFail = new boolean[3];
        this.machineInfoRefBoxDao = machineInfoRefBoxDao;
        this.productionTimesConfig = productionTimesConfig;

        executing = initRobotFlags();
        starting = true;
        stop = false;
        this.robotClientWrapper = robotClientWrapper;
        this.worldKnowledge = preProductionService.getWorldKnowledge();
    }

    private TemporalNode[] initRobotFlags() {
        final TemporalNode[] executing;
        executing = new TemporalNode[3];
        robotStopped = new boolean[3];
        replanRequest = new int[3];
        timerStarting = new long[3];
        robotStopped[0] = false;
        robotStopped[1] = true;
        robotStopped[2] = true;

        cleanTask[0] = false;
        cleanTask[1] = false;
        cleanTask[2] = false;
        makeCleanTaskFail[0] = false;
        makeCleanTaskFail[1] = false;
        makeCleanTaskFail[2] = false;
        return executing;
    }

    private void setRobotStoppedInitially() {
        if (this.robotClient.getRobotCount() >= 2) {
            robotStopped[1] = false;
            if (this.robotClient.getRobotCount() >= 3)
                robotStopped[2] = false;
        }
    }


    synchronized
    private void tryDispathecLcked(TemporalEdge e) {
        e.enable();
        tgraph.updateEdge(tgraph.getSource(e), tgraph.getTarget(e), e);
        tryDispatchNode(tgraph.getTarget(e),tgraph.getSource(e));
    }


    @SneakyThrows
    public void calculatePlan(boolean simplestPlan) {
        if (!simplestPlan) {
            this.kb.generateInitialKB();
            this.distanzeMatrix.generateDistanceMatrix();
        }



        PlanningThread winningThread = new PlanningThread("1",  800,
                simplestPlan, kb, robotClient.getRobotCount(), productionTimesConfig, machineInfoRefBoxDao,
                refboxClient.getTeamColor().orElseThrow(), distanzeMatrix.getDistanzMatrix(), refboxClient);
        winningThread.plan();


        if (winningThread != null) {
            ImmutablePair<List<Order>, ImmutablePair<TemporalGraph, RcllCodedProblem>> winningResult;
            winningResult = winningThread.getResult();
            updateWinningResult(winningResult);

            System.out.println(winningThread.getPlanString());

            kb.setCp(cp);
            kb.generateOrderKb(this.goals);
        } else {
            calculatePlan(true);
        }
    }

    private void updateWinningResult(ImmutablePair<List<Order>, ImmutablePair<TemporalGraph, RcllCodedProblem>> winningResult) {
        this.goals = winningResult.left;
        this.tgraph = winningResult.right.left;
        this.cp = winningResult.right.right;
    }



    @Override
    public void handleBeaconSignal(BeaconSignalProtos.BeaconSignal  beacon_signal, int robotId, Peer robot) {
        synchronized (this) {
            if (!refboxClient.isReadyForProduction()) {
                log.info("Not yet ready for production, refbox client is missing stuff!");
                return;
            }


            if (first && refboxClient.getAllNoDsPreparedOrders().size() >= minOrdersToPlan && robotId == 1) {

                first = false;
                calculatePlan(false);
                this.actionMapping.updateCodedProblem(cp.getConstants());
                dispatch();
            }
        }
    }

    private void resetAllMachines() {
        this.refboxClient.sendResetMachine(Machine.CS1);
        this.refboxClient.sendResetMachine(Machine.CS2);
        this.refboxClient.sendResetMachine(Machine.RS1);
        this.refboxClient.sendResetMachine(Machine.RS2);
        this.refboxClient.sendResetMachine(Machine.BS);
        this.refboxClient.sendResetMachine(Machine.DS);
    }

    @Override
    public boolean handleRobotTaskResult(AgentTasksProtos.AgentTask prsTask) {
        if (prsTask.hasRetrieve() ) {
            return this.handleGetResult(prsTask.getTaskId(), prsTask.getRobotId(), prsTask.getSuccessful());
        } else if (prsTask.hasDeliver()) {
            return this.handleDeliverResult(prsTask.getTaskId(), prsTask.getRobotId(), prsTask.getSuccessful());
        }  else if (prsTask.hasMove()) {
            return this.handleMoveToWaypointResult(prsTask.getTaskId(), prsTask.getRobotId(), prsTask, prsTask.getSuccessful());
        } else if (prsTask.hasBuffer()) {
            return this.handleBufferCapStationResult(prsTask.getTaskId(), prsTask.getRobotId(), prsTask.getBuffer(), prsTask.getSuccessful());
        } else {
            log.error("Unsupported midlevelTaskResult: " + prsTask.toString());
            return false;
        }
    }

    private boolean handleBufferCapStationResult(int taskId, int robotId, AgentTasksProtos.BufferStation bufferCapStation, boolean wasSucess) {
        SubProductionTask subTask = subProductionTaskDao.findByIdAndRobotId(taskId, robotId);
        if (subTask == null) {
            log.error("Error, task with result was not found!");
            return false;
        } else if (wasSucess) {
            subTask.setState(SUCCESS, "robot said MoveToTask was SUCCESS");
            subProductionTaskDao.save(subTask);
            log.info("Buffer Task: " + taskId + " was finished by robot: " + robotId);

            log.warn("SUCCESS FOR A BUFFER TASK");

            Set<TemporalEdge> outEdges = tgraph.getOutputEdges(executing[robotId - 1]);
            Iterator<TemporalEdge> it = outEdges.iterator();
            while (it.hasNext()) {
                TemporalEdge e = it.next();
                if (e.getType() == 2) {
                    dispatchEndNode(e);
                }
            }

        } else {
            log.error("MoveTo Buffer Failed result: " + wasSucess + " for task: " + taskId + " unassigning it and retrying it...");
            subTask.setState(TBD, "Retrying after FAIL");
            subProductionTaskDao.save(subTask);   //ATAI: HERE REPLANNING AFTER FAILURE CAN BE CALLED

        }
        return true;
    }

    private boolean handleMoveToWaypointResult(Integer taskId, Integer robotId, AgentTasksProtos.AgentTask moveToWaypoint, boolean wasSucess) {
        SubProductionTask subTask = subProductionTaskDao.findByIdAndRobotId(taskId, robotId);
        if (subTask == null || taskId.longValue() == initalWaitingTaskId) {
            log.error("Error, task with result was not found!");
            return false;
        }

        if (wasSucess) {
            handleSuccessfulMoveTaskResult(taskId, robotId, subTask);
        } else {
            handleFailedMoveTaskResult(taskId, robotId, moveToWaypoint, wasSucess, subTask);
        }
        return true;
    }

    private void handleFailedMoveTaskResult(Integer taskId, Integer robotId, AgentTasksProtos.AgentTask moveToWaypoint, boolean wasSucess, SubProductionTask subTask) {
        log.error("MoveTo Task Failed result: " + wasSucess + " for task: " + taskId + " unassigning it and retrying it...");
        subTask.setState(TBD, "Retrying after FAIL");

        subProductionTaskDao.save(subTask);

        synchronized (this) {

            if (!cleanTask[robotId - 1]) {
                log.warn("Reassigning MOVE task");

                BeaconSignalFromRobot robotPos = beaconSignalFromRobotDao.findFirstByRobotIdOrderByLocalTimestampDesc(Integer.toString(robotId));

                String colorprefix = refboxClient.isCyan() ? "C_" : "M_";
                double startingPoint = 4.2;
                if (leaveStartingZoneTask && robotPos.getPoseY() < 1 && ((robotPos.getPoseX() < -4.2 && colorprefix.equals("M_")) || (robotPos.getPoseX() > 4.2 && colorprefix.equals("C_")))) {
                    log.warn("Reassigning MOVE task to leave starting area");
                    robotClient.sendPrsTaskToRobot(moveToWaypoint);
                } else {
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    try {
                        robotClientWrapper.sendProductionTaskToRobot(subTask);
                    } catch (Exception e) {
                        log.warn("cleanup task succeded, ignore previous log");
                    }
                }
            } else {


                if (!makeCleanTaskFail[robotId - 1]) {
                    log.warn("cleanup task for robot " + (robotId - 1) + " failed unexpectely, retring");
                    robotClientWrapper.sendProductionTaskToRobot(subTask);
                } else {  //if makeCleanTaskFail is true, another task is expected to come (so I do not set the robot as stopped)

                    log.warn("cleanup task failed after request");
                    cleanTask[robotId - 1] = false;
                    makeCleanTaskFail[robotId - 1] = false;


                }

            }
        }
    }

    private void handleSuccessfulMoveTaskResult(Integer taskId, Integer robotId, SubProductionTask subTask) {
        subTask.setState(SUCCESS, "robot said MoveToTask was SUCCESS");
        subProductionTaskDao.save(subTask);
        log.info("MoveTo Task: " + taskId + " was finished by robot: " + robotId);

        synchronized (this) {

            if (robotId == 2 && firstR2 && leaveStartingZoneTask) {
                firstR2 = false;
                log.info("robot 2 exited the starting zone");
                robotStopped[robotId - 1] = true;
                log.info("SETTING ROBOT STOPPED " + robotId + " TO TRUE INSIDE waitingtaskfinished");
                Set<TemporalEdge> outEdges = tgraph.getOutputEdges(tgraph.getNode(-1));
                Iterator<TemporalEdge> it = outEdges.iterator();
                while (it.hasNext()) {
                    TemporalEdge e = it.next();
                    tryDispathecLcked(e);
                }
            } else if (robotId == 3 && firstR3 && leaveStartingZoneTask) {
                firstR3 = false;
                log.info("robot 3 exited the starting zone");
                robotStopped[robotId - 1] = true;
                log.info("SETTING ROBOT STOPPED " + robotId + " TO TRUE INSIDE waitingtaskfinished");
                Set<TemporalEdge> outEdges = tgraph.getOutputEdges(tgraph.getNode(-2));
                Iterator<TemporalEdge> it = outEdges.iterator();
                while (it.hasNext()) {
                    TemporalEdge e = it.next();
                    tryDispathecLcked(e);
                }
            } else if (cleanTask[robotId - 1]) {

                if (makeCleanTaskFail[robotId - 1]) {
                    log.warn("Tried to cancel a cleantask of robot " + robotId + " but the canceling failed, another move action incoming, inside management of task " + taskId);
                    cleanTask[robotId - 1] = false;
                    makeCleanTaskFail[robotId - 1] = false;
                } else {
                    log.warn("cleanup task successfully completed");
                    cleanTask[robotId - 1] = false;
                    robotStopped[robotId - 1] = true;
                    log.info("SETTING ROBOT STOPPED " + robotId + " TO TRUE INSIDE cleantaskend");
                }

            } else {
                log.info("Managing dispatch of end node for task " + taskId + " and robot " + robotId);
                Set<TemporalEdge> outEdges = tgraph.getOutputEdges(executing[robotId - 1]);
                Iterator<TemporalEdge> it = outEdges.iterator();
                while (it.hasNext()) {
                    TemporalEdge e = it.next();
                    if (e.getType() == 2) {
                        dispatchEndNode(e);
                    }
                }
            }

        }
    }

    public boolean handleDeliverResult(long taskId, int robotId, boolean wasSuccess) {
        SubProductionTask subTask = subProductionTaskDao.findByIdAndRobotId(taskId, robotId);

        if (subTask == null) {
            log.error("Error, task that should fail was not found!");
            return false;
        } else if (wasSuccess) {

            subTask.setState(SUCCESS_PENDING, "robot said task was SUCCESS");
            subProductionTaskDao.save(subTask);
            log.info("Deliver Task: " + taskId + " was supposedly finished by robot: " + robotId);


            // no prepare required
            if (subTask.getMachine().contains("RS")) {
                subTask.setState(SUCCESS, "deliver was done to slide");
                subProductionTaskDao.save(subTask);

            }

            if (subTask.getMachine().contains("DS")) {
                subTask.setState(SUCCESS, "deliver was done to slide");
                subProductionTaskDao.save(subTask);

            }


            log.warn("SUCCESS FOR A DELIVERY TASK");

            Set<TemporalEdge> outEdges = tgraph.getOutputEdges(executing[robotId - 1]);
            Iterator<TemporalEdge> it = outEdges.iterator();
            while (it.hasNext()) {
                TemporalEdge e = it.next();
                if (e.getType() == 2) {
                    dispatchEndNode(e);
                }
            }
        } else {
            log.error("Deliver Failed result: " + wasSuccess + " for task: " + taskId + " unassigning it and retrying it...");
            subTask.setState(TBD, "Retrying after FAIL");
            subProductionTaskDao.save(subTask);     //ATAI: HERE REPLANNING AFTER FAILURE CAN BE CALLED
        }
        return true;
    }


    public boolean handleGetResult(long taskId, int robotId, boolean wasSuccess) {
        SubProductionTask subTask = subProductionTaskDao.findByIdAndRobotId(taskId, robotId);

        if (subTask == null) {
            log.error("Error, task that should fail was not found!");
            return false;
        } else if (wasSuccess) {
            subTask.setState(SUCCESS, "robot said SUCCESS");
            log.warn("SUCCESS FOR GET task" + taskId);
            subProductionTaskDao.save(subTask);


            //todo manage successfull task
            Set<TemporalEdge> outEdges = tgraph.getOutputEdges(executing[robotId - 1]);
            Iterator<TemporalEdge> it = outEdges.iterator();
            while (it.hasNext()) {
                TemporalEdge e = it.next();
                if (e.getType() == 2) {
                    dispatchEndNode(e);
                }
            }

        } else {
            log.error("GET Failed result: " + wasSuccess + " for task: " + taskId + ", replanning");
            subTask.setState(TBD, "Retrying after FAIL");
            subProductionTaskDao.save(subTask);     //ATAI: HERE REPLANNING AFTER FAILURE CAN BE CALLED
        }
        return true;
    }






    @Override
    public void handleMachineInfo(MachineInfoProtos.Machine machine) {
        List<SubProductionTask> task = subProductionTaskDao.findByMachineAndState(machine.getName(), SubProductionTask.TaskState.SUCCESS_PENDING);
        for (SubProductionTask subProductionTask : task) {
            MachineName name = new MachineName(subProductionTask.getMachine());
            MachineState state = refboxClient.getStateForMachine(name.asMachineEnum()).get();
            if (state.equals(MachineState.READY_AT_OUTPUT)) {
                subProductionTaskDao.save(subProductionTask);
            }
        }
    }

    public void assignTaskToRobot(SubProductionTask task, int robotId) {
        if (task.getName().equals("Move")) assignMoveTaskToRobot(task,robotId);
        else assignNonMoveTaskToRobot(task,robotId);
    }


    public void assignMoveTaskToRobot(SubProductionTask task, int robotId) {

        task.setRobotId(robotId);
        subProductionTaskDao.save(task);

        synchronized (this) {
            if (cleanTask[robotId - 1]) {
                makeCleanTaskFail[robotId - 1] = true;
                robotClient.cancelTask(robotId);
            }
        }

        if (cleanTask[robotId - 1]) {
            try {
                Thread.sleep(7000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        String locName = task.getMachine().toLowerCase(Locale.ROOT).substring(2);
        if (!locName.equals("bs") && !locName.equals("ds")) {
            MachineSide side = task.getSide();
            if (side==MachineSide.Slide || side==MachineSide.Shelf)
                side = MachineSide.Input;
            locName = locName + "_" + side.toString().toLowerCase(Locale.ROOT);
        }

        robotClientWrapper.sendProductionTaskToRobot(task);

        timerStarting[robotId - 1] = getLatestProductionTime();


        if (starting) {
            starting = false;
        }


    }




    public void assignNonMoveTaskToRobot(SubProductionTask task, int robotId) {
        log.info("TASK TO EXECUTE BY ROBOT ID " + robotId + ", checking station state");
        //task.setRobotId(robotId);
        String colorprefix = refboxClient.isCyan() ? "C-" : "M-";
        if (task.getMachine() != colorprefix + "DS") {
            do {
                Optional<MachineState> stateOptional = refboxClient.getStateForMachine(new MachineName(task.getMachine()).asMachineEnum());
                if (stateOptional.isPresent()) {
                    MachineState state = stateOptional.get();
                    if (state.equals(MachineState.DOWN) || state.equals(MachineState.BROKEN)) {
                        log.warn("Machine " + task.getMachine() + " is down or broken , waiting 5 seconds");
                        try {
                            Thread.sleep(5000);
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                    } else {
                        break;
                    }
                } else {
                    log.warn("State not available for: " + task.getMachine());
                }
            } while (true);
        }
        task.setState(SubProductionTask.TaskState.INWORK, "set to INWORK due to assigning task to robot");
        subProductionTaskDao.save(task);
        robotClientWrapper.sendProductionTaskToRobot(task);
    }

    public void goToWaitZone(int robotId) {
        SubProductionTask moveTask = SubProductionTaskBuilder.newBuilder()
                .setName("MoveToZone")
                .setState(SubProductionTask.TaskState.INWORK)
                .setType(SubProductionTask.TaskType.MOVE)
                .setOrderInfoId(null)
                .build();
        //moveTask.setId(taskId);
        moveTask.setRobotId(robotId);
        subProductionTaskDao.save(moveTask);
        robotClientWrapper.sendMoveToZoneTask((long) robotId, moveTask.getId(), gameField.getWaitingZone(robotId));
    }




    public void dispatch() {
        timerStarting[0] = getLatestProductionTime();
        timerStarting[1] = getLatestProductionTime();
        timerStarting[2] = getLatestProductionTime();

        //get the plan starting node
        TemporalNode planStart = tgraph.getNode(0);
        tryDispatchNode(planStart,planStart);

    }

    public void tryDispatchNode(TemporalNode node, TemporalNode source) {
        if (!stop) {
            if (threadDebug) {
                if (source.getType() == 3 || source.getType() == 3) {
                    log.info("start checking support of node " + node.getName() + " coming from " + source.getName());
                }
            }
            boolean ready = true;
            Set<TemporalEdge> inEdges = tgraph.getInputEdges(node);
            Iterator<TemporalEdge> it = inEdges.iterator();
            while (it.hasNext() && ready) {
                TemporalEdge e = it.next();
                if (!e.getEnabled()) {
                    ready = false;
                }
            }
            if (threadDebug) {
                if (source.getType() == 3 || source.getType() == 3) {
                    log.info("finish checking support " + ready + " of node " + node.getName() + " coming from " + source.getName());
                }
            }
            if (ready && node.getType() != 2) {
                if (node.getType() != 3) {
                    synchronized (this) {
                        SubProductionTask task = actionMapping.actionToTask(tgraph.nodeToAction(node.getTime(), node.getId()));
                        int robotId = task.getRobotId();
                        robotStopped[robotId - 1] = false;
                        log.info("SETTING ROBOT STOPPED " + robotId + " TO FALSE INSIDE tryDispatchNode");
                    }
                }
                //dispatching the node
                dispatchNode(node,source);
                //enabling outpud edges
                Set<TemporalEdge> outEdges = tgraph.getOutputEdges(node);
                it = outEdges.iterator();
                while (it.hasNext()) {
                    TemporalEdge e = it.next();
                    if (e.getLb() == minValue) {              //simplest case, without lower bound on the edge
                        tryDispathecLcked(e);
                    }
                }

            } else {
                if (threadDebug) {
                    if (source.getType() == 3 || source.getType() == 3) {
                        log.info("not supported  node " + node.getName() + " coming from " + source.getName());
                    }
                }
            }
        }
    }

    public void dispatchEndNode(TemporalEdge incomingEdge) {
        TemporalNode node = tgraph.getTarget(incomingEdge);
        if (node.isValid()) {
            boolean ready;
            Iterator<TemporalEdge> it;
            synchronized (this) {
                incomingEdge.enable();
                tgraph.updateEdge(tgraph.getSource(incomingEdge), tgraph.getTarget(incomingEdge), incomingEdge);
                ready = true;
                Set<TemporalEdge> inEdges = tgraph.getInputEdges(node);
                it = inEdges.iterator();
                while (it.hasNext() && ready) {
                    TemporalEdge e = it.next();
                    if (!e.getEnabled()) {
                        ready = false;
                    }
                }
            }
            if (ready) {
                double time = node.getTime();
                IntOp action = tgraph.nodeToAction(time, node.getId());
                //ATAI: CHECK THE ENDING PRECONDITIONS ON THE KB HERE
                //ATAI: APPLY THE ENDING EFFECTS ON THE KB HERE

                //enabling outpud edges
                SubProductionTask task = actionMapping.actionToTask(action);
                int robotId = task.getRobotId();
                long remainingTime = productiontime - getLatestProductionTime();
                long c0time = 31 + 45 + 30 + 30 + 30 + 25 + 25 * 7;
                ;
                long actualTime = getLatestProductionTime();
                if (!stop) {
                    synchronized (this) {
                        robotStopped[robotId - 1] = true;
                        log.info("SETTING ROBOT STOPPED " + robotId + " TO TRUE INSIDE dispatchEndNode");
                    }
                    Set<TemporalEdge> outEdges = tgraph.getOutputEdges(node);
                    it = outEdges.iterator();
                    while (it.hasNext()) {
                        TemporalEdge e = it.next();
                        if (e.getLb() == minValue) {              //simplest case, without lower bound on the edge
                            tryDispathecLcked(e);
                        }
                    }

                    if (robotStopped[0] && robotStopped[1] && robotStopped[2]) {
                        log.info("Plan dispatching end, starting to plan for new orders");
                    } else {
                        if (robotStopped[0])
                            log.info("robot 1 idling");
                        if (robotStopped[1])
                            log.info("robot 2 idling");
                        if (robotStopped[2])
                            log.info("robot 3 idling");
                    }

                } else { //receive a signal to stop dispatching, stopping the robot

                    log.info("stopping robot " + robotId);
                    synchronized (this) {
                        robotStopped[robotId - 1] = true;
                        log.info("SETTING ROBOT STOPPED " + robotId + " TO TRUE INSIDE dispatchEndNode2");
                        if (robotStopped[0] && robotStopped[1] && robotStopped[2])
                            log.info("++++++ ALL ROBOT STOPPED!!!!");
                    }
                }
            }
            if (!ready)
                log.info("+++++++++++ERROR IN ACTION ENDING+++++");
        }
    }


    synchronized
    public void dispatchNode(TemporalNode node,TemporalNode source) {
        if (node.isValid()) {
            int type = node.getType();
            double time = node.getTime();
            if (type == 1) {
                IntOp action = tgraph.nodeToAction(time, node.getId());

                //ATAI: CHECK THE STARTING PRECONDITIONS ON THE KB HERE

                tgraph.checkOrderHardDeadlines(node, getLatestProductionTime());
                log.info("Time: " + time + " Timer: " + getLatestProductionTime() + " - Dispatching action " + action.prettyPrint(cp));
                SubProductionTask task = actionMapping.actionToTask(action);
                int robotId = task.getRobotId();
                executing[robotId - 1] = node;
                if (GamePhase.PostGame.equals(refboxClient.getGamePhase())) {
                    log.info("Game is finished, not dispensing new task!");
                    return;
                }
                synchronized (this) {
                    while (!robotStartChecker.checkIfShouldStart(robotId)) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                    }
                    //ATAI: UPDATE THE STARTING EFFECTS ON THE KB HERE
                    assignTaskToRobot(task, robotId);
                }


            } else if (type == 2) { //end action
                log.error("error");
            }
        } else {
            log.error ("Invalid node");
        }
    }

    public String getPlan() {
        if (this.winningThread != null) {
            return this.winningThread.getPlanString();
        }
        return "";
    }

    public Integer getPlanTime() {
        return -1;
    }

    private long getLatestProductionTime() {
        return refboxClient.getLatestGameTimeInSeconds();
    }
}
