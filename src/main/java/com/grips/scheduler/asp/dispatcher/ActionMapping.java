package com.grips.scheduler.asp.dispatcher;


import com.grips.config.RefboxConfig;
import com.grips.persistence.domain.ProductOrder;
import com.grips.persistence.domain.SubProductionTask;
import com.grips.persistence.domain.SubProductionTaskBuilder;
import com.rcll.domain.*;
import com.rcll.planning.encoding.IntOp;
import com.rcll.refbox.RefboxClient;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.MachineDescriptionProtos;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

@CommonsLog
public class ActionMapping {

    private final RefboxClient refboxClient;
    private List<String> cpConstants;

    private String colorprefix;

    public ActionMapping(RefboxClient refboxClient, RefboxConfig refboxConfig) {
        this.refboxClient = refboxClient;
    }

    public void updateCodedProblem(List<String> cpConstants) {
        this.cpConstants = cpConstants;
    }

    public SubProductionTask actionToTask(IntOp action) {
        String colorprefix = refboxClient.getTeamColor().map(x -> TeamColor.CYAN.equals(x) ? "C-" : "M-").orElseThrow();

        String name = action.getName();
        SubProductionTask task;
        int orderIdIndex;
        int orderId;
        int robotIndex = action.getInstantiations()[0];
        int robotId = Character.getNumericValue(cpConstants.get(robotIndex).charAt(1));
        switch (name) {
            case "move":
                int stationIndex;
                String station;
                int locationIndex = action.getInstantiations()[2];
                String stationAndSide = cpConstants.get(locationIndex);
                String side = "";
                if (stationAndSide.equals("bs") || stationAndSide.equals("ds")) {
                    station = colorprefix + stationAndSide.toUpperCase(Locale.ROOT);
                    if (stationAndSide.equals("bs"))
                        side = "output";
                    else
                        side = "input";
                } else {
                    station = stationAndSide.substring(0, stationAndSide.indexOf('_'));
                    station = colorprefix + station.toUpperCase(Locale.ROOT);
                    side = stationAndSide.substring(stationAndSide.indexOf('_') + 1);
                }
                MachineSide mside;
                if (side.equals("input"))
                    mside = MachineSide.Input;
                else
                    mside = MachineSide.Output;

                if (action.getSpecialInput() == 1)
                    mside = MachineSide.Shelf;
                else if (action.getSpecialInput() == 2)
                    mside = MachineSide.Slide;

                task = SubProductionTaskBuilder.newBuilder()
                        .setName("Move")
                        .setMachine(station)
                        .setState(SubProductionTask.TaskState.INWORK)
                        .setType(SubProductionTask.TaskType.MOVE)
                        .setSide(mside)
                        .setOrderInfoId(null)
                        .setIsDemandTask(false)
                        .build();
                break;

            case "waitForDeliveryWindow":
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("wait")
                        .setOptCode(null)
                        .build();
                break;

            case "getBaseFromBScriticalTask":
                // parameters (?r - robot ?bs - base_station ?p - product)
                orderIdIndex = action.getInstantiations()[2];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(1));
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("getBaseFromBScriticalTask")
                        .setMachine(colorprefix + "BS")                 //ATAI: machine type can be BS, RS, CS or DS
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.GET)        //ATAI: task type can be GET, DELIVER, BUFFER_CAP or MOVE (look in AspScheduler class for creation of move task. Right now they are automatically created.
                        .setSide(MachineSide.Output)
                        .setOrderInfoId((long) orderId)
                        .setOptCode(null)
                        .build();
                break;

            case "getBaseFromBSResourceTask":
                // parameters (?r - robot ?bs - base_station )
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("getBaseFromBSResourceTask")
                        .setMachine(colorprefix + "BS")                 //ATAI: machine type can be BS, RS, CS or DS
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.GET)        //ATAI: task type can be GET, DELIVER, BUFFER_CAP or MOVE (look in AspScheduler class for creation of move task. Right now they are automatically created.
                        .setSide(MachineSide.Output)
                        .setOptCode(null)
                        .build();
                break;


            case "deliverProductC0ToCScriticalTask":
                //parameters (?r - robot ?csi - cap_station_input  ?cs - cap_station ?p - product ?c - ccolor)
                orderIdIndex = action.getInstantiations()[3];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(1));
                //TODO RINBGCOLOR

                task = SubProductionTaskBuilder.newBuilder()
                        .setName("deliverProductC0ToCScriticalTask")
                        .setMachine(colorprefix + "CS")                 //ATAI: machine type can be BS, RS, CS or DS
                        .setState(SubProductionTask.TaskState.INWORK)
                        .setType(SubProductionTask.TaskType.DELIVER)        //ATAI: task type can be GET, DELIVER, BUFFER_CAP or MOVE (look in AspScheduler class for creation of move task. Right now they are automatically created.
                        .setSide(MachineSide.Input)
                        .setOrderInfoId((long) orderId)
                        .setOptCode(MachineDescriptionProtos.CSOp.MOUNT_CAP.toString())
                        //.setRequiredColor()
                        .build();
                break;

            case "getCapBaseFromCSresourceTask": //Get Base+Cap at shel near CS input for buffering
                // parameters (?r - robot  ?csi - cap_station_input ?cs - cap_station )
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("getCapBaseFromCSresourceTask")
                        .setMachine(colorprefix + "CS")                 //ATAI: machine type can be BS, RS, CS or DS
                        .setState(SubProductionTask.TaskState.INWORK)
                        .setType(SubProductionTask.TaskType.BUFFER_CAP)        //ATAI: task type can be GET, DELIVER, BUFFER_CAP or MOVE (look in AspScheduler class for creation of move task. Right now they are automatically created.
                        .setSide(MachineSide.Input)
                        .setOptCode("RETRIEVE_CAP")
                        .setIsDemandTask(true)
                        .build();
                break;

            case "getBaseFromCSResourceTask": //Get Base after buffering to dispose it
                // parameters (?r - robot ?cso - cap_station_output ?cs - cap_station)
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("getBaseFromCSResourceTask")
                        .setMachine(colorprefix + "CS")                 //ATAI: machine type can be BS, RS, CS or DS
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.GET)        //ATAI: task type can be GET, DELIVER, BUFFER_CAP or MOVE (look in AspScheduler class for creation of move task. Right now they are automatically created.
                        .setSide(MachineSide.Output)
                        .setIsDemandTask(true)
                        .build();
                break;

            case "getProductFromCScriticalTask":
                //parameters (?r - robot ?cso - cap_station_output ?cs - cap_station ?p - product )
                orderIdIndex = action.getInstantiations()[3];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(1));


                task = SubProductionTaskBuilder.newBuilder()
                        .setName("getProductFromCScriticalTask")
                        .setMachine(colorprefix + "CS")                 //ATAI: machine type can be BS, RS, CS or DS
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.GET)        //ATAI: task type can be GET, DELIVER, BUFFER_CAP or MOVE (look in AspScheduler class for creation of move task. Right now they are automatically created.
                        .setSide(MachineSide.Output)
                        .setOrderInfoId((long) orderId)
                        .setOptCode(null)
                        .build();
                break;

            case "deliverProductToDScriticalTask":
                //parameters (?r - robot ?ds - delivery_station ?p - product)
                orderIdIndex = action.getInstantiations()[2];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(1));
                Order order = refboxClient.getOrderById(orderId);

                // Create the SubProductionTask
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("getProductFromDScriticalTask")
                        .setMachine(colorprefix + "DS")                 //ATAI: machine type can be BS, RS, CS or DS
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.DELIVER)        //ATAI: task type can be GET, DELIVER, BUFFER_CAP or MOVE (look in AspScheduler class for creation of move task. Right now they are automatically created.
                        .setOrderInfoId((long) orderId)
                        .setOptCode(order.getDeliveryGate()+"")
                        .build();
                break;

            //ATAI parameters that have to be set for specific kind of tasks:
            //.setOrderInfoId((long) orderId) in all tasks in which you are manipulating a subpiece of the product (buffering cs, retrieving the base from the CS after the buffering, retrieving a base for a RS and delivering a base to the RS slide are excluded)
            //.setIsDemandTask(true) for all the tasks excluded by .setOrderInfoId (buffering cs, retrieving the base from the CS after the buffering, retrieving a base for a RS and delivering a base to the RS slide)
            //.setRequiredColor for retrieving a base, delivering partial product to RS to mount a ring, delivering partial product to CS to mount a cap
            //.setOptCode("RETRIEVE_CAP") for the buffer cap action
            //.setOptCode(MachineDescriptionProtos.CSOp.MOUNT_CAP.toString()) when delivering a partial product to a CS to mount a cap
            //.setOptCode(order.getDeliveryGate()+"") when delivering the final product to the DS (order is a ProductOrder object stored in the ProductOrderDAO)

            /***
             case "toyactiondeliveringfrombs":
             break;

             case "deliverProductC1ToCScriticalTask":
             break;

             case "deliverProductC2ToCScriticalTask":
             break;

             case "deliverProductC3ToCScriticalTask":
             break;

             case "deliverProductToRS1criticalTask":
             break;

             case "deliverProductToRS2criticalTask":
             break;

             case "deliverProductToRS3criticalTask":
             break;

             case "getProductFromRSCriticalTask":
             break;

             case "getProductFromRSCriticalTask":
             break;

             case "deliverBaseToRSResourceTask":
             break;
             **/



            default:
                System.out.println("case not managed");
                task = null;
                break;
        }
        task.setRobotId(robotId);
        return task;
    }
}
