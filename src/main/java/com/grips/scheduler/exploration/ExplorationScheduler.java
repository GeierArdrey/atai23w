package com.grips.scheduler.exploration;

import com.grips.config.ExplorationConfig;
import com.grips.config.StationsConfig;
import com.grips.persistence.dao.*;
import com.grips.persistence.domain.*;
import com.grips.scheduler.asp.RobotStartChecker;
import com.rcll.domain.MachineName;
import com.rcll.domain.Peer;
import com.rcll.domain.ZoneName;
import com.rcll.refbox.RefboxClient;
import com.grips.scheduler.api.IScheduler;
import com.rcll.protobuf_lib.RobotConnections;
import com.grips.scheduler.GameField;
import com.robot_communication.services.GripsRobotClient;
import com.robot_communication.services.GripsRobotTaskCreator;
import com.shared.domain.Point2d;
import com.shared.domain.ZoneExplorationState;
import lombok.NonNull;
import lombok.Synchronized;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.*;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@CommonsLog
@Service
public class ExplorationScheduler implements IScheduler {
    private final int NUMBER_OF_MACHINES = 4; //16
    private final Map<Integer, Boolean> recentAssigned;

    private final GameField gameField;
    private final RefboxClient refboxClient;

    private final RobotConnections _robotConnections;

    private final ExplorationTaskDao _explorationTaskDao;
    private final GripsRobotTaskCreator robotTaskCreator;
    private final GripsRobotClient robotClient;
    private final BeaconSignalFromRobotDao beaconSignalFromRobotDao;
    private final MachineExplorationService explorationService;
    private final RobotObservationDao robotObservationDao;

    private final PreProductionService preProductionService;

    private final SubProductionTaskDao subProductionTaskDao;
    private final HashMap<Long, AgentTasksProtos.AgentTask> _activeExplorationTasks = new HashMap<>();
    //todo upgrade!
    private HashMap<Peer, AgentTasksProtos.AgentTask> _previousExplorationTask = new HashMap<>();

    private final StationsConfig stationsConfig;

    private final RobotStartChecker robotStartChecker;

    private boolean robot1DidRotation = false;

    private Stack<String> zones;

    public ExplorationScheduler(GameField gameField,
                                RobotConnections robotConnections,
                                ExplorationTaskDao explorationTaskDao,
                                GripsRobotTaskCreator robotTaskCreator,
                                ExplorationConfig explorationConfig,
                                RefboxClient refboxClient,
                                GripsRobotClient robotClient,
                                BeaconSignalFromRobotDao beaconSignalFromRobotDao,
                                MachineExplorationService explorationService,
                                RobotObservationDao robotObservationDao,
                                PreProductionService preProductionService,
                                SubProductionTaskDao subProductionTaskDao,
                                StationsConfig stationsConfig,
                                RobotStartChecker robotStartChecker) {
        this.refboxClient = refboxClient;
        this.robotObservationDao = robotObservationDao;
        this.preProductionService = preProductionService;
        this.robotStartChecker = robotStartChecker;
        this.subProductionTaskDao = subProductionTaskDao;
        this.stationsConfig = stationsConfig;
        log.info("Using exploration config: " + explorationConfig.toString());
        this.gameField = gameField;
        this.zones = new Stack<>();
        this.zones.push("C_Z13_waiting");
        this.zones.push("M_Z13_waiting");
        this.zones.push("C_Z55_waiting");
        this.zones.push("M_Z55_waiting");


        this.recentAssigned = new ConcurrentHashMap<>();
        this.recentAssigned.put(1, false);
        this.recentAssigned.put(2, false);
        this.recentAssigned.put(3, false);

        _robotConnections = robotConnections;
        _explorationTaskDao = explorationTaskDao;
        this.robotTaskCreator = robotTaskCreator;
        this.robotClient = robotClient;
        this.beaconSignalFromRobotDao = beaconSignalFromRobotDao;
        this.explorationService = explorationService;

        this.robotObservationDao.deleteAll();

    }

    // if a robot is active, assign a new task, otherwise return 0
    @Synchronized
    private AgentTasksProtos.AgentTask getNextExplorationTask(@NonNull Integer robotId) {
        AgentTasksProtos.AgentTask newTask = null;
        Peer robot = _robotConnections.getRobot(robotId);
        //newTask = getBestExplorationOption(robot);
        newTask = getUnexplored(robot);
        if (newTask == null) {
            newTask = getRandomExplorationTask(robotId);
        }
        return newTask;
    }

    private AgentTasksProtos.AgentTask getUnexplored(Peer robot) {
        BeaconSignalFromRobot beacon = beaconSignalFromRobotDao.findFirstByRobotIdOrderByLocalTimestampDesc(Long.toString(robot.getId()));
        Point2d robotPose = new Point2d(beacon.getPoseX(), beacon.getPoseY());
        Optional<ExplorationZone> zone = Optional.empty();
        if (robot.getId() == 1) {
            zone = this.gameField.getAllZones().stream()
                    .filter(x -> x.getExplorationState() == ZoneExplorationState.UNEXPLORED).min((a, b) -> {
                        double distA = a.getZoneCenter().distance(robotPose);
                        double distB = b.getZoneCenter().distance(robotPose);
                        if (distA < distB) {
                            return 1;
                        } else if (distB < distA) {
                            return -1;
                        }
                        return 0;
                    });
        } else if (robot.getId() == 3) {
            List<ExplorationZone> zones = this.gameField.getAllZones().stream()
                    .filter(x -> x.getExplorationState() == ZoneExplorationState.UNEXPLORED).sorted((a, b) -> {
                        double distA = a.getZoneCenter().distance(robotPose);
                        double distB = b.getZoneCenter().distance(robotPose);
                        if (distA < distB) {
                            return 1;
                        } else if (distB < distA) {
                            return -1;
                        }
                        return 0;
                    }).collect(Collectors.toList());
            zone = Optional.of(zones.get(zones.size() / 4));
        } else if (robot.getId() == 2){
            List<ExplorationZone> zones = this.gameField.getAllZones().stream()
                    .filter(x -> x.getExplorationState() == ZoneExplorationState.UNEXPLORED).sorted((a, b) -> {
                        double distA = a.getZoneCenter().distance(robotPose);
                        double distB = b.getZoneCenter().distance(robotPose);
                        if (distA < distB) {
                            return 1;
                        } else if (distB < distA) {
                            return -1;
                        }
                        return 0;
                    }).collect(Collectors.toList());
            zone = Optional.of(zones.get(zones.size() / 2));
        } else {
            throw new RuntimeException("Unkown Robot: " + robot.getId());
        }
        if (zone.isPresent()) {
            zone.get().setExplorationState(ZoneExplorationState.SCHEDULED);
            return robotTaskCreator.createExplorationTask(robot.getId(), "XXX", zone.get().getZoneName() + "_waiting", true);
        }
        log.warn("No zone found for: " + robot.getId());
        return null;
    }

    ;

    private AgentTasksProtos.AgentTask getRandomExplorationTask(long robotId) {
        if (zones.empty()) {
            return null;
        }
        return robotTaskCreator.createExplorationTask(robotId, "XXX", zones.pop(), true);
    }

    @Synchronized
    public void explorationTaskResult(long robotId, @NonNull final MachineName machineName, String zoneName) {
    }

    public void explorationTaskFailed(long robotId, MachineName machineName, String zoneName) {
        log.warn("Exploration task failed[" + robotId + "] for: " + machineName + " in: " + zoneName);
        this._activeExplorationTasks.remove(robotId);
    }

    private AgentTasksProtos.AgentTask getBestExplorationOption(@NonNull Peer robot) {
        // first see, if there are observations by this robot
        List<RobotObservation> usedObservations = this.explorationService.getUsedObservations(robot.getId());

        // now we try to find the nearest observation
        // first we check, if the robot has a previous task
        Point2d prevZoneCenter = null;
        AgentTasksProtos.AgentTask prevTask = _previousExplorationTask.get(robot);
        if (prevTask != null) {
            //System.out.println("Previous task found for position estimation");
            String zoneId = prevTask.getExploreMachine().getWaypoint();
            ExplorationZone prevZone = gameField.getZoneByName(zoneId);
            prevZoneCenter = prevZone.getZoneCenter();

            if (prevZoneCenter == null) {
                log.warn("Error that should not happen, check!!!");
                prevZoneCenter = new Point2d(0, 0);
            }
        } else {
            prevZoneCenter = new Point2d(0, 0);
        }
        RobotObservation obs = findNearestObservationNotScheduled(usedObservations, prevZoneCenter);

        AgentTasksProtos.AgentTask prsTask = null;
        if (obs != null) {
            prsTask = robotTaskCreator.createExplorationTask(robot.getId(), obs.getMachineName().getRawMachineName(), obs.getMachineZone() + "_exploration_" + obs.getOrientation(), false);
            log.info("Sending Robot " + robot.getId() + " to explore Machine " + prsTask.getExploreMachine().getMachineId()
                    + " at Zone " + prsTask.getExploreMachine().getWaypoint());
        }
        return prsTask;
    }

    private RobotObservation findNearestObservationNotScheduled(List<RobotObservation> usedObservations, Point2d prevZoneCenter) {
        log.info("Searching nearest observation: " + usedObservations.size() + " observations found, pos is: " + prevZoneCenter.toString());
        double minDistance = Double.MAX_VALUE;
        RobotObservation nearestObservation = null;

        for (RobotObservation obs : usedObservations) {
            ExplorationZone zone = gameField.getZoneByName(obs.getMachineZone());
            if (zone == null) {
                continue;
            }
            double distance = zone.getZoneCenter().distance(prevZoneCenter);
            boolean alreadyScheduled = isAlreadyScheduled(obs);
            log.info("Distance to " + zone.getZoneName() + " is : " + distance + " alreadyScheduled: " + alreadyScheduled);
            if (distance < minDistance && !alreadyScheduled) {
                minDistance = distance;
                nearestObservation = obs;
            }
        }

        return nearestObservation;
    }

    private boolean isAlreadyScheduled(RobotObservation obs) {
        boolean alreadyScheduled = _activeExplorationTasks.values().stream().anyMatch(t -> t.getExploreMachine().getWaypoint().equalsIgnoreCase(obs.getMachineZone()));
        //todo because of challanges  String mirroredZone = _gameField.getZoneByName(obs.getMachineZone()).getMirroredZone().getZoneName();
        // alreadyScheduled |= _activeExplorationTasks.values().stream()
        //         .anyMatch(t -> t.getExploreMachine().getWaypoint().equalsIgnoreCase(mirroredZone));
        alreadyScheduled |= _activeExplorationTasks.values().stream()
                .anyMatch(t -> new MachineName(t.getExploreMachine().getMachineId()).equals(obs.getMachineName()));
        alreadyScheduled |= _activeExplorationTasks.values().stream()
                .anyMatch(t -> new MachineName(t.getExploreMachine().getMachineId()).equals(obs.getMachineName().mirror()));

        return alreadyScheduled;
    }

    @Override
    public void handleBeaconSignal(BeaconSignalProtos.BeaconSignal beacon_signal, int robotId, Peer robot) {
        this.gameField.markExploredByPosition(new Point2d(beacon_signal.getPose().getX(), beacon_signal.getPose().getY()));
        this.gameField.markExploredByPosition(new Point2d(-beacon_signal.getPose().getX(), beacon_signal.getPose().getY()));

        if (this.recentAssigned.get(robotId)) {
            this.recentAssigned.put(robotId, false);
            log.warn("Robot Recently got a task, skipping beacon signal!");
            return;
        }
        // immediately assign tasks to robot1, wait for robot2 and robot3
        if (beacon_signal.getTask().getTaskId() == -1 && robotStartChecker.checkIfShouldStart(robotId)) {
            //robot currently has no task assigned
            AgentTasksProtos.AgentTask task = null;
            task = preProductionService.getPreProductionTask(robotId);
            if (task == null) {
                task = getNextExplorationTask(robotId);
            }

            if (task != null) {
                SubProductionTask subProductionTask = new SubProductionTask();
                subProductionTask.setRobotId((int) robot.getId());
                subProductionTask.setName("Explore_" + task.getMove().getWaypoint());
                subProductionTask.setId(task.getTaskId());

                subProductionTaskDao.save(subProductionTask);
                _activeExplorationTasks.put(robot.getId(), task);
                ExplorationTask eTask = new ExplorationTask();
                eTask.setRobotId(robotId);
                eTask.setMachine(task.getExploreMachine().getMachineId());
                eTask.setZone(task.getExploreMachine().getWaypoint());
                eTask.setTimeStamp(System.currentTimeMillis());
                _explorationTaskDao.save(eTask);
                robotClient.sendPrsTaskToRobot(task);
                this.recentAssigned.put(robotId, true);
            } else {
                log.warn("No task for Robot " + robotId + " found in exploration!");
            }
        } else {
            log.debug("robot already has a task assigned, do not assign a new one");
        }
        this.reportAllMachines();
        // we also broadcast all already successfully reported machines to all robots
        //sendSuccessfullMachineReports.sendSuccessfullMachineReports(robotId);
    }

    private void reportAllMachines() {
        if (!refboxClient.getTeamColor().isPresent()) {
            return;
        }
        stationsConfig.getUsedStations(refboxClient.getTeamColor().get()).forEach(station -> {
            List<RobotObservation> obs = this.robotObservationDao.findAllByMachineName(station);
            Map<String, List<RobotObservation>> equalObservations = obs.stream().collect(Collectors.groupingBy(RobotObservation::identifier));
            equalObservations.forEach((a, b) -> {
                Set<Long> seenByRobots = b.stream().map(RobotObservation::getRobotId).collect(Collectors.toSet());
                Integer counter = b.stream().mapToInt(RobotObservation::getCount).sum();
                if (counter > 0 || seenByRobots.size() > 0) {
                    MachineName name = b.get(0).getMachineName();
                    int rotation = b.get(0).getOrientation();
                    ZoneName zone = new ZoneName(b.get(0).getMachineZone());

                    ZoneName z = zone;
                    this.gameField.getAllZones().stream()
                            .filter(x -> x.getZoneNameSdk().equals(z) || x.getZoneNameSdk().equals(z.mirror()))
                            .forEach(x -> {
                                if (x.getExplorationState() != ZoneExplorationState.EXPLORED) {
                                    log.info("Marking zone " + x.getZoneName() + " as EXPLORED");
                                    x.setExplorationState(ZoneExplorationState.EXPLORED);
                                }
                            });
                    log.info("REPORTING: " + name);
                    explorationService.markMachineAsReported(name, zone);
                    explorationService.markMachineAsReported(name.mirror(), zone);
                    this.refboxClient.sendReportMachine(name, zone, rotation);
                } else {
                    log.warn("SKIPPING: " + a);
                }
            });

        });
    }

    private boolean checkPose(double poseX1, double poseY1) {
        if (poseY1 < 1.2)
            return false;
        else {
            double pointX = refboxClient.isCyan() ? 4.5 : -4.5;
            double pointY = 0.5;
            double distance = calculateDistance(poseX1, poseY1, pointX, pointY);
            if (distance < 1) {
                return false;
            } else {
                log.info("distance between the two robots is " + distance);
                return true;
            }
        }
    }

    private double calculateDistance(double poseX1, double poseY1, double poseX2, double poseY2) {
        return Math.sqrt(((poseX1 - poseX2) * (poseX1 - poseX2)) + ((poseY1 - poseY2) * (poseY1 - poseY2)));
    }


    @Override
    public boolean handleRobotTaskResult(AgentTasksProtos.AgentTask prsTask) {
        log.info("handeling task result: " + prsTask);
        this._activeExplorationTasks.remove((long) prsTask.getRobotId());
        return true;
    }

    @Override
    public void handleMachineInfo(MachineInfoProtos.Machine machine) {
        if (machine.hasZone()) {
            this.preProductionService.handleMachineInfo(machine);
        }
    }

    public void explorationTaskSucess(AgentTasksProtos.AgentTask prsTask) {
        log.info("explorationTaskResult: robotId: " + prsTask.toString());
        explorationService.markMachineAsInactive(new MachineName(prsTask.getExploreMachine().getMachineId()));
        this._activeExplorationTasks.remove((long) prsTask.getRobotId());
    }

    public boolean anyActiveTasks() {
        this._activeExplorationTasks.forEach((id, t) -> log.info("Active Task: " + t));
        return preProductionService.anyTask() || !this._activeExplorationTasks.isEmpty();
    }

    public void cancelAllTask() {
        this.robotClient.cancelOnAll();
    }
}
