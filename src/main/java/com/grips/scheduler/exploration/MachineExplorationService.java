package com.grips.scheduler.exploration;

import com.google.common.collect.Lists;
import com.grips.scheduler.GameField;
import com.rcll.domain.MachineName;
import com.grips.persistence.dao.MachineReportDao;
import com.grips.persistence.dao.RobotObservationDao;
import com.grips.persistence.domain.MachineReport;
import com.grips.persistence.domain.RobotObservation;
import com.rcll.domain.ZoneName;
import com.shared.domain.Point2d;
import lombok.Synchronized;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@CommonsLog
@Service
public class MachineExplorationService {
    private final RobotObservationDao robotObservationDao;
    private final MachineReportDao machineReportDao;

    private List<RobotObservation> notVerifiedObservations = new ArrayList<>();
    private List<MachineName> alreadyReportedMachines = new ArrayList<>();

    private final GameField gameField;

    public MachineExplorationService(RobotObservationDao robotObservationDao, MachineReportDao machineReportDao, GameField gameField) {
        this.robotObservationDao = robotObservationDao;
        this.machineReportDao = machineReportDao;
        this.gameField = gameField;
        notVerifiedObservations.addAll(Lists.newArrayList(this.robotObservationDao.findAll()));
        Iterable<MachineReport> machineReports = this.machineReportDao.findAll();
        for (MachineReport mReport : machineReports) {
            alreadyReportedMachines.add(mReport.getName());
        }
    }

    @Synchronized
    public void updateRobotObservation(RobotObservation observation) {
        //log.info("Adding Observation: " + observation);
        if (alreadyReportedMachines.contains(observation.getMachineName())) {
            return;
        }
        Optional<RobotObservation> old = robotObservationDao.findAllByRobotId(observation.getRobotId()).stream()
                .filter(x -> x.isLogicalSameNoRobot(observation))
                .findFirst();
        if (old.isPresent()) {
            old.get().setCount(old.get().getCount() + 1);
            robotObservationDao.save(old.get());
        } else {
            notVerifiedObservations.add(observation);
            robotObservationDao.save(observation);
        }
    }


    //todo think of cancelling ongoing exploration tasks that are not longer needed!
    @Synchronized
    public void markMachineAsReported(MachineName machineName, ZoneName zone) {
        alreadyReportedMachines.add(machineName);
        alreadyReportedMachines.add(machineName.mirror());
        float x = Integer.parseInt(String.valueOf(zone.toString().charAt(3)))  - 0.5f;
        float y = Integer.parseInt(String.valueOf(zone.toString().charAt(4))) - 0.5f;
        Point2d p = new Point2d(x, y);
        this.gameField.markExploredByPosition(p);
        p.setX(p.getX() * -1);
        this.gameField.markExploredByPosition(p);

    }

    @Synchronized
    public void markMachineAsInactive(MachineName machineName) {
        alreadyReportedMachines.add(machineName);
        alreadyReportedMachines.add(machineName.mirror());
    }


    @Synchronized
    public int getReportedMachines() {
        return alreadyReportedMachines.size();
    }

    @Synchronized
    public int getNotVerifiedObservationsSize() {
        return notVerifiedObservations.size();
    }

    @Synchronized
    public List<RobotObservation> getNotVerifiedObservations() {
        return notVerifiedObservations;
    }

    @Synchronized
    public boolean clearZone(long robotId, MachineName machineName, String zoneName) {
        // remove either own observation or other observation
        if (!machineName.isDummyMachine()) {
            // we where observing a machine
            Optional<RobotObservation> ownObs = notVerifiedObservations.stream()
                    .filter(o -> o.getRobotId() == robotId
                            && o.getMachineName().equals(machineName)
                            && o.getMachineZone().equalsIgnoreCase(zoneName))
                    .findAny();
            if (ownObs.isPresent()) {
                log.info("Removing robot's own observation for machine " + machineName + " at zone " + zoneName);
                notVerifiedObservations.remove(ownObs.get());
            }

            Optional<RobotObservation> anyObs = notVerifiedObservations.stream()
                    .filter(o -> o.getMachineName().equals(machineName) && o.getMachineZone().equalsIgnoreCase(zoneName))
                    .findAny();
            if (anyObs.isPresent()) {
                log.info("Removing random own observation for machine " + machineName + " at zone " + zoneName);
                notVerifiedObservations.remove(anyObs.get());
            }
        } else {
            return true;
        }
        return false;
    }

    @Synchronized //todo could be that 2 robots get one assigned, but I don't think that happen.
    public List<RobotObservation> getUsedObservations(long robotId) {
        List<RobotObservation> usedObservations = new LinkedList<>();
        List<RobotObservation> ownObservations = notVerifiedObservations.stream()
                .collect(Collectors.toList());
        if (ownObservations.size() > 0) {
            return ownObservations;
        }
        log.error("No Free Observation!");
        return Collections.emptyList();
    }

    public void markZoneViaRobotPosition(double x, double y) {

    }
}
