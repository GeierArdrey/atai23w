package com.grips.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "gameconfig.production")
public class ProductionConfig {
    private String grey_cap_machine;
    private String black_cap_machine;

    private boolean doC0;
    private boolean doC1;
    private boolean doC2;
    private boolean doC3;

    private boolean doStandingC0SS;

    private int maxSimultaneousProducts;
    private int maxSimultaneousProductsSameCap;
}
