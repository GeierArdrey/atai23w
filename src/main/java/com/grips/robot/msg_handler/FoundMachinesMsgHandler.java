package com.grips.robot.msg_handler;

import com.grips.persistence.domain.RobotObservation;
import com.grips.scheduler.exploration.MachineExplorationService;
import com.grips.tools.DiscretizeAngles;
import com.rcll.refbox.RefboxClient;
import lombok.extern.apachecommons.CommonsLog;
import org.dozer.Mapper;
import org.robocup_logistics.llsf_msgs.GripsExplorationFoundMachinesProtos;
import org.robocup_logistics.llsf_msgs.RobotMachineReportProtos;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;

@Service
@CommonsLog
public class FoundMachinesMsgHandler implements Consumer<GripsExplorationFoundMachinesProtos.GripsExplorationFoundMachines> {
    private final Mapper mapper;
    private final MachineExplorationService machineExplorationService;

    private final RefboxClient refboxClient;

    public FoundMachinesMsgHandler(Mapper mapper,
                                   MachineExplorationService machineExplorationService,
                                   RefboxClient refboxClient) {
        this.mapper = mapper;
        this.machineExplorationService = machineExplorationService;
        this.refboxClient = refboxClient;
    }

    @Override
    public void accept(GripsExplorationFoundMachinesProtos.GripsExplorationFoundMachines foundMachines) {
        long robot_id = foundMachines.getRobotId();
        for (RobotMachineReportProtos.RobotMachineReportEntry tmp_mre : foundMachines.getMachinesList()) {
            //int rotation = (int)(tmp_mre.getRotation()*180.0/Math.PI) + 180;
            //int rotation = (int)(tmp_mre.getRotation()*180.0/Math.PI);
            int rotation = tmp_mre.getRotation();
            rotation = (rotation % 360 + 360) % 360;
            rotation = DiscretizeAngles.discretizeAngles(rotation);

            //          System.out.println("REPORT machine: " + tmp_mre.getName() + " with orientation: " + tmp_mre.getRotation() + " and side: " + tmp_mre.getSide() + " from lidar: " + tmp_mre.getFromLidar());

            RobotObservation observation = mapper.map(tmp_mre, RobotObservation.class);
            observation.setRobotId(robot_id);
            observation.setObservationTimeGame(refboxClient.getLatestGameTimeInNanoSeconds());
            observation.setObservationTimeAbsolut(refboxClient.getLatestGameTimeInNanoSeconds());
            observation.setMachineZoneConfidence(1.0);
            observation.setOrientation(rotation);
            observation.setMachineZone(tmp_mre.getZone().toString());
            observation.setCount(0);
            this.machineExplorationService.updateRobotObservation(observation);
            // is saved in explorationscheduler
            //robotObservationDao.save(observation);
        }
    }
}
