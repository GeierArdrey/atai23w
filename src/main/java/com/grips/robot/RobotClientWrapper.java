package com.grips.robot;

import com.grips.scheduler.api.LockPartService;
import com.rcll.domain.MachineName;
import com.grips.persistence.domain.SubProductionTask;
import com.grips.scheduler.api.DbService;
import com.rcll.domain.MachineSide;
import com.robot_communication.services.GripsRobotClient;
import lombok.NonNull;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Service;

@Service
@CommonsLog
public class RobotClientWrapper {
    private final GripsRobotClient robotClient;
    private final LockPartService lockPartService;

    private int materialCountCS [];

    public RobotClientWrapper(GripsRobotClient robotClient,
                              LockPartService lockPartService) {
        this.robotClient = robotClient;
        this.lockPartService = lockPartService;
        materialCountCS = new int[2];
        materialCountCS[0] = 0;
        materialCountCS[1] = 0;
    }

    private void sendBufferCap(Long robotId, long taskId, String machine) {
        int csID;
        if (machine.equals("C-CS1") || machine.equals("M-CS1"))
            csID = 0;
        else
            csID = 1;
        if (materialCountCS[csID] == 3) {
            materialCountCS[csID] = 0;
        }
        materialCountCS[csID]++;
        this.robotClient.sendBufferCap(robotId, taskId, new MachineName(machine), materialCountCS[csID]);
    }

    private void sendMoveTask(Long robotId, long taskId, MachineName machine, MachineSide side) {
        this.robotClient.sendMoveTask(robotId, taskId, machine, side);
    }

    public void sendMoveToZoneTask(Long robotId, long taskId, String zone) {
        this.robotClient.sendMoveToZoneTask(robotId,taskId,zone);
    }

    public void sendGetTask(@NonNull Long robotId,
                            @NonNull Long taskId,
                            @NonNull String machine,
                            MachineSide side) {
        Integer count = lockPartService.getMaterialCount(machine + "_" + MachineSide.Shelf.toString());
        this.robotClient.sendGetTaskToRobot(robotId, taskId, machine, side, count);
    }

    public void sendProductionTaskToRobot(@NonNull SubProductionTask task) {
        log.info("Task: " + task.getId() + " will be send to Robot " + task.getRobotId());
        switch (task.getType()) {
            case MOVE:
                this.sendMoveTask(Long.valueOf(task.getRobotId()), task.getId(), new MachineName(task.getMachine()), task.getSide());
                break;
            case BUFFER_CAP:
                this.sendBufferCap(Long.valueOf(task.getRobotId()), task.getId(), task.getMachine());
                break;
            case GET:
                this.sendGetTask(Long.valueOf(task.getRobotId()), task.getId(), task.getMachine(), task.getSide());
                break;
            case DELIVER:
                Integer count = lockPartService.getMaterialCount(task.getMachine() + "_" + MachineSide.Shelf.toString());
                this.robotClient.sendDeliverTaskToRobot(Long.valueOf(task.getRobotId()), task.getId(), task.getMachine(), task.getSide(), count);
                break;
            case DUMMY:
                this.robotClient.sendDummyTaskToRobot(Long.valueOf(task.getRobotId()), task.getMachine(), task.getSide());
                break;
            default:
                log.error("Task[" + task.getId() + "] type not handeled: " + task.getType());
        }

    }
}
