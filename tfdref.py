#!/usr/bin/python2.7

import sys, getopt, datetime

#for each action, I specify which attributes of the macro have to taken for the first action and which attributes for the second action
def attributesMapping(): 
   firstAttributes = {}
   secondAttributes = {}
   firstAttributes["move_getcapbasefromcsresourcetask"] = [0,3,1]
   secondAttributes["move_getcapbasefromcsresourcetask"] = [0,1,2]

   firstAttributes["move_getbasefrombscriticaltask"] = [0,3,1]
   secondAttributes["move_getbasefrombscriticaltask"] = [0,1,2]

   firstAttributes["move_deliverproducttors1criticaltask"] = [0,4,2]
   secondAttributes["move_deliverproducttors1criticaltask"] = [0,1,2,3,5,6,7,8]

   firstAttributes["move_deliverproducttors2criticaltask"] = [0,4,2]
   secondAttributes["move_deliverproducttors2criticaltask"] = [0,1,2,3,5,6,7,8]

   firstAttributes["move_deliverproducttors3criticaltask"] = [0,4,2]
   secondAttributes["move_deliverproducttors3criticaltask"] = [0,1,2,3,5,6,7,8]

   firstAttributes["move_deliverbasetorsresourcetask"] = [0,3,2]
   secondAttributes["move_deliverbasetorsresourcetask"] = [0,1,2,4,5]

   firstAttributes["move_getproductfromrscriticaltask"] = [0,4,2]
   secondAttributes["move_getproductfromrscriticaltask"] = [0,1,2,3]

   firstAttributes["move_deliverproductc0tocscriticaltask"] = [0,4,1]
   secondAttributes["move_deliverproductc0tocscriticaltask"] = [0,1,2,3,5] 

   firstAttributes["move_deliverproductc1tocscriticaltask"] = [0,4,1]
   secondAttributes["move_deliverproductc1tocscriticaltask"] = [0,1,2,3,5]  

   firstAttributes["move_deliverproductc2tocscriticaltask"] = [0,4,1]
   secondAttributes["move_deliverproductc2tocscriticaltask"] = [0,1,2,3,5] 

   firstAttributes["move_deliverproductc3tocscriticaltask"] = [0,4,1]
   secondAttributes["move_deliverproductc3tocscriticaltask"] = [0,1,2,3,5]  

   firstAttributes["move_getproductfromcscriticaltask"] = [0,4,1]
   secondAttributes["move_getproductfromcscriticaltask"] = [0,1,2,3] 

   firstAttributes["move_deliverproducttodscriticaltask"] = [0,3,1]
   secondAttributes["move_deliverproducttodscriticaltask"] = [0,1,2] 

   firstAttributes["move_getbasefrombsresourcetask"] = [0,2,1]
   secondAttributes["move_getbasefrombsresourcetask"] = [0,1]

   firstAttributes["move_getbasefromcsresourcetask"] = [0,3,1]
   secondAttributes["move_getbasefromcsresourcetask"] = [0,1,2]

   firstAttributes["move_getwastedproductfromrs"] = [0,4,2]
   secondAttributes["move_getwastedproductfromrs"] = [0,1,2,3]

   firstAttributes["move_getwastedproductfromcs"] = [0,4,1]
   secondAttributes["move_getwastedproductfromcs"] = [0,1,2,3]

   firstAttributes["move_deliverwastedproducttorsresourcetask"] = [0,4,2]
   secondAttributes["move_deliverwastedproducttorsresourcetask"] = [0,1,2,3,5,6]

   return firstAttributes, secondAttributes

def printTimePoint (plan, key, writing):
  actions = plan[key]
  for a in actions:
     writing.write(str(key) + ": " + str(a) + "\n")



def createRawFile(inputfile,outputfile):
  firstAttributes, secondAttributes = attributesMapping()
  reading = open(inputfile,'r')
  writing = open(outputfile,'w')  
  lines = reading.readlines()
  plan = {}
  minValue = 0.000001
  for line in lines:
   if not "waitfordeliverywindow" in line:
      time = float(line.split(":")[0])
      preactions = []
      if plan.has_key(time):
         preactions = plan[time]
      action = line.split(":")[1][:-1]
      durationStr = action.split("[")[1].split("]")[0]
      duration = float(durationStr)

      
      actionName = action.split("(")[1].split(" ")[0]
      attributes = action.split("(")[1].split(")")[0].split(" ")[1:]

      interactionDuration = 20
      
      if "getcapbase" in actionName:
         interactionDuration = 25
      
      moveDuration = duration - interactionDuration





      firstAction = "(move"
      for attr in firstAttributes[actionName]:
         firstAction = firstAction + " " + attributes[attr]

      if "getcapbase" in actionName:
      	newToPosition = firstAction.split(" ")[-1].replace("input","shelf")
        firstAction = firstAction.replace(firstAction.split(" ")[-1],newToPosition)

      if "deliverbasetorsresourcetask" in actionName or "deliverwastedproducttorsresourcetask" in actionName:
      	newToPosition = firstAction.split(" ")[-1].replace("input","slide")
        firstAction = firstAction.replace(firstAction.split(" ")[-1],newToPosition)
      
      firstAction = firstAction + ") [" + str(moveDuration - minValue) + "]"



      preactions.append(firstAction)
      plan[time] = preactions



      
      postactions = []
      posttime = time + moveDuration 
      if plan.has_key(posttime):
         postactions = plan[posttime]

      secondAction = "(" + actionName.replace("move_","")
      for attr in secondAttributes[actionName]:
         secondAction = secondAction + " " + attributes[attr]
      secondAction = secondAction + ") [" + str(interactionDuration) + "]"


      postactions.append(secondAction)       
      plan[posttime] = postactions
  for key in sorted(plan):
   printTimePoint(plan, key, writing)
   #writing.write(str(key) + ":" + str(plan[key]) + "\n")
   print(str(key) + ":" + str(plan[key])) 


def main(argv):
   createRawFile(argv[0],argv[1])



if __name__ == "__main__":
   main(sys.argv[1:])
